﻿/*************************************************************************
 * Copyright (C) 2017 <SWG Vision of Hope>
 *
 * This file is part of the SWG Vision of Hope Launcher.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *************************************************************************/
// @author: Iosnowore

using SWGVOHLauncher.forms.panel;
using SWGVOHLauncher.src.services.font;
using SWGVOHLauncher.src.services.sound;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace SWGVOHLauncher.forms.optimize
{
    public partial class OptimizeDialog : Form
    {
        private readonly List<string> FilesToRemove;
        private readonly List<long> FileSizes;
        private readonly List<string> FoldersToRemove;
        private SidePanel panel;

        public OptimizeDialog(List<string> FilesToRemove, List<long> FileSizes, List<string> FoldersToRemove, SidePanel panel)
        {
            InitializeComponent();
            this.FilesToRemove = FilesToRemove;
            this.FileSizes = FileSizes;
            this.FoldersToRemove = FoldersToRemove;
            this.panel = panel;
            OptimizeDescription.Font = CustomFontService.CustomFontFont(OptimizeDescription);
            OptimizeButton.Font = CustomFontService.CustomFontFont(OptimizeButton);
            FileListBox.Font = CustomFontService.CustomFontFont(FileListBox);
            AllCheckBox.Font = CustomFontService.CustomFontFont(AllCheckBox);

            for (int i = 0; i < FilesToRemove.Count; i++)
                FileListBox.Items.Add(FilesToRemove[i] + "\t" + (FileSizes[i] < 1000 ? FileSizes[i] + "kb" : FileSizes[i] / 1024 + "mb"));
            for (int i = 0; i < FoldersToRemove.Count; i++)
                FileListBox.Items.Add(FoldersToRemove[i] + "\t? mb");
        }

        private void AllCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < FileListBox.Items.Count; i++)
                FileListBox.SetItemChecked(i, AllCheckBox.Checked);
        }

        private void OptimizeButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("Button_Click", panel);

            for (int i = 0; i < FileListBox.Items.Count; i++)
            {
                if (FileListBox.GetItemChecked(i))
                {
                    if (FilesToRemove.Count > i && File.Exists(FilesToRemove[i]))
                        File.Delete(FilesToRemove[i]);
                    else if (FoldersToRemove.Count > i && Directory.Exists(FoldersToRemove[i]))
                        Directory.Delete(FoldersToRemove[i], true);
                }
            }
            MessageBox.Show("Game directory has been successfully optimized!", "All selected files have been purged");
            Dispose();
        }
    }
}
