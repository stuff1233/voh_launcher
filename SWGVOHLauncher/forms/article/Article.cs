﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using SWGVOHLauncher.src.services.font;
using SWGVOHLauncher.src.custom;
using SWGVOHLauncher.src.services.sound;

namespace SWGVOHLauncher.forms.article
{
    public partial class Article : UserControl
    {
        private string Url;
        private readonly CustomLabel TitleLabel = new CustomLabel();
        private readonly CustomLabel DescriptionLabel = new CustomLabel();
        private static LauncherMain LauncherMain;

        public Article(string Title, string Description, string Url, int index, LauncherMain LauncherMain)
        {
            InitializeComponent();
            
            TitleLabel.Text = Title;
            DescriptionLabel.Text = Description;
            this.Url = "https://swghope.com/forums/showthread.php?" + Url;

            TitleLabel.Font = CustomFontService.CustomFontFont(30);
            DescriptionLabel.Font = CustomFontService.CustomFontFont(18);
            TitleLabel.Location = new Point((Width - TitleLabel.Width) / 2 - 175, 260);
            DescriptionLabel.Location = new Point((Width - DescriptionLabel.Width) / 2 - 175, 310);
            TitleLabel.MouseClick += TitleLabel_MouseClick;
            DescriptionLabel.MouseClick += DescriptionLabel_MouseClick;
            Article.LauncherMain = LauncherMain;
            Controls.Add(TitleLabel);
            Controls.Add(DescriptionLabel);

            if (index == 0)
            {
                Location = new Point(25, 125);
                BackgroundImage = Properties.Resources.article;
            }
            else
            {
                Location = new Point(560, 125);
                BackgroundImage = Properties.Resources.article1;
            }
        }

        private void DescriptionLabel_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", LauncherMain.GetSidePanel());
            Process.Start(Url);
        }

        private void TitleLabel_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", LauncherMain.GetSidePanel());
            Process.Start(Url);
        }

        private void Article_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", LauncherMain.GetSidePanel());
            Process.Start(Url);
        }

        public void SetTitle(string Title)
        {
            TitleLabel.Text = Title;
        }

        private void Article_MouseEnter(object sender, EventArgs e)
        {
            if (LauncherMain.GetActiveArticle() != null)
                return;
            LauncherMain.SetActiveArticle(this);
            if (LauncherMain.ProgressBarIsFull() || !LauncherMain.IsReadyToPlay())
                SoundService.PlaySound("Sound_Hover", LauncherMain.GetSidePanel());
            BorderStyle = BorderStyle.Fixed3D;
        }
    }
}
