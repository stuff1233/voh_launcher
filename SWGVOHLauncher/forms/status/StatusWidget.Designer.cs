﻿namespace SWGVOHLauncher.forms.status
{
    partial class StatusWidget
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StatusTitleLabel = new System.Windows.Forms.Label();
            this.PopulationLabel = new System.Windows.Forms.Label();
            this.UptimeLabel = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.HighestPopulationLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // StatusTitleLabel
            // 
            this.StatusTitleLabel.AutoSize = true;
            this.StatusTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.StatusTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusTitleLabel.ForeColor = System.Drawing.Color.White;
            this.StatusTitleLabel.Location = new System.Drawing.Point(0, 3);
            this.StatusTitleLabel.Name = "StatusTitleLabel";
            this.StatusTitleLabel.Size = new System.Drawing.Size(85, 29);
            this.StatusTitleLabel.TabIndex = 0;
            this.StatusTitleLabel.Text = "Status:";
            // 
            // PopulationLabel
            // 
            this.PopulationLabel.AutoSize = true;
            this.PopulationLabel.BackColor = System.Drawing.Color.Transparent;
            this.PopulationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PopulationLabel.ForeColor = System.Drawing.Color.White;
            this.PopulationLabel.Location = new System.Drawing.Point(128, 3);
            this.PopulationLabel.Name = "PopulationLabel";
            this.PopulationLabel.Size = new System.Drawing.Size(179, 29);
            this.PopulationLabel.TabIndex = 1;
            this.PopulationLabel.Text = "Population: 100";
            // 
            // UptimeLabel
            // 
            this.UptimeLabel.AutoSize = true;
            this.UptimeLabel.BackColor = System.Drawing.Color.Transparent;
            this.UptimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UptimeLabel.ForeColor = System.Drawing.Color.White;
            this.UptimeLabel.Location = new System.Drawing.Point(0, 3);
            this.UptimeLabel.Name = "UptimeLabel";
            this.UptimeLabel.Size = new System.Drawing.Size(449, 29);
            this.UptimeLabel.TabIndex = 2;
            this.UptimeLabel.Text = "Uptime: 6 days, 12 hours, and 24 minutes";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.BackColor = System.Drawing.Color.Transparent;
            this.StatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusLabel.ForeColor = System.Drawing.Color.Green;
            this.StatusLabel.Location = new System.Drawing.Point(65, 3);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(84, 29);
            this.StatusLabel.TabIndex = 3;
            this.StatusLabel.Text = "Online";
            // 
            // HighestPopulationLabel
            // 
            this.HighestPopulationLabel.AutoSize = true;
            this.HighestPopulationLabel.BackColor = System.Drawing.Color.Transparent;
            this.HighestPopulationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HighestPopulationLabel.ForeColor = System.Drawing.Color.White;
            this.HighestPopulationLabel.Location = new System.Drawing.Point(274, 3);
            this.HighestPopulationLabel.Name = "HighestPopulationLabel";
            this.HighestPopulationLabel.Size = new System.Drawing.Size(267, 29);
            this.HighestPopulationLabel.TabIndex = 4;
            this.HighestPopulationLabel.Text = "Highest Population: 150";
            // 
            // StatusWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.Controls.Add(this.HighestPopulationLabel);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.UptimeLabel);
            this.Controls.Add(this.PopulationLabel);
            this.Controls.Add(this.StatusTitleLabel);
            this.Name = "StatusWidget";
            this.Size = new System.Drawing.Size(515, 35);
            this.Load += new System.EventHandler(this.StatusWidget_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label StatusTitleLabel;
        private System.Windows.Forms.Label PopulationLabel;
        private System.Windows.Forms.Label UptimeLabel;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Label HighestPopulationLabel;
    }
}
