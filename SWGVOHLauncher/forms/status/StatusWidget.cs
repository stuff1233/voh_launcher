﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SWGVOHLauncher.src.services.font;
using SWGVOHLauncher.src.services.Reader;

namespace SWGVOHLauncher.forms.status
{
    public partial class StatusWidget : UserControl
    {
        private int Type;

        public StatusWidget(int Type)
        {
            InitializeComponent();

            this.Type = Type;
            Control[] ControlsToUpdate = {
                StatusTitleLabel,
                StatusLabel,
                PopulationLabel,
                UptimeLabel,
                HighestPopulationLabel
            };

            foreach (Control control in ControlsToUpdate)
                control.Font = CustomFontService.CustomFontFont(control);

            if (Type == 0)
            {
                UptimeLabel.Visible = false;
                StatusLabel.Text = WebClientReader.GetWebPageContent("https://geniusgaming.net/status/indominus/status.txt");
                PopulationLabel.Text = "Population: " + WebClientReader.GetWebPageContent("https://geniusgaming.net/status/indominus/pop.txt");
                HighestPopulationLabel.Text = "Highest Population: " + WebClientReader.GetWebPageContent("https://geniusgaming.net/status/indominus/hpop.txt");
                PopulationLabel.Location = new Point(StatusLabel.Location.X + StatusLabel.Width, PopulationLabel.Location.Y);
                HighestPopulationLabel.Location = new Point(PopulationLabel.Location.X + PopulationLabel.Width, HighestPopulationLabel.Location.Y);
                switch (StatusLabel.Text)
                {
                    case "online":
                        StatusLabel.ForeColor = Color.Green;
                        break;
                    case "loading":
                        StatusLabel.ForeColor = Color.Orange;
                        break;
                    default:
                        StatusLabel.ForeColor = Color.Red;
                        break;
                }
            }
            else
            {
                int StartTime = int.Parse(WebClientReader.GetWebPageContent("https://geniusgaming.net/status/indominus/start.txt"));
                DateTime UnixStartTime = DateTimeOffset.FromUnixTimeSeconds(StartTime).DateTime;
                DateTime UtcTime = DateTime.UtcNow;
                TimeSpan Uptime = UtcTime - UnixStartTime;
                UptimeLabel.Text = "uptime: ";
                if (Uptime.Days > 0)
                {
                    UptimeLabel.Text += Uptime.Days + " day";
                    if (Uptime.Days > 1)
                        UptimeLabel.Text += "s";
                }
                    
                if (Uptime.Hours > 0)
                {
                    if (!UptimeLabel.Text.Equals("uptime: "))
                        UptimeLabel.Text += ", ";
                    UptimeLabel.Text += Uptime.Hours + " hour";
                    if (Uptime.Hours > 1)
                        UptimeLabel.Text += "s";
                }
                    
                if (Uptime.Minutes > 0)
                {
                    if (!UptimeLabel.Text.Equals("uptime: "))
                        UptimeLabel.Text += ", ";
                    UptimeLabel.Text += Uptime.Minutes + " minute";
                    if (Uptime.Minutes > 1)
                        UptimeLabel.Text += "s";
                }
                UptimeLabel.Location = new Point((Width - UptimeLabel.Width) / 2, UptimeLabel.Location.Y);
                StatusTitleLabel.Visible = false;
                StatusLabel.Visible = false;
                PopulationLabel.Visible = false;
                HighestPopulationLabel.Visible = false;
            }
        }

        private void StatusWidget_Load(object sender, EventArgs e)
        {
            Location = new Point(Type == 0 ? 25 : 560, 85);
        }
    }
}
