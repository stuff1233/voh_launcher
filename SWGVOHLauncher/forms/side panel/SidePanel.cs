﻿/*************************************************************************
 * Copyright (C) 2017 <SWG Vision of Hope>
 *
 * This file is part of the SWG Vision of Hope Launcher.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *************************************************************************/
// @author: Iosnowore

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using SWGVOHLauncher.forms.optimize;
using SWGVOHLauncher.forms.tools;

using SWGVOHLauncher.src.services.config;
using SWGVOHLauncher.src.services.font;
using SWGVOHLauncher.src.services.game;
using SWGVOHLauncher.src.services.movement;
using SWGVOHLauncher.src.services.patching;
using SWGVOHLauncher.src.services.sound;

namespace SWGVOHLauncher.forms.panel
{
    public partial class SidePanel : UserControl
    {
        public SidePanel()
        {
            InitializeComponent();
            BackColor = Color.Transparent;
            Location = new Point(10, 150);

            Control[] ControlsToUpdate = {
                AnimationsCheckBox,
                BackLabel,
                ChangeGameDirButton,
                ClientSetupButton,
                DisableCutScreensCheckBox,
                EnableFXCheckBox,
                GameDirectoryLabel,
                gameDirectoryTitleLabel,
                GameDirOutput,
                GraphicsCheckBox,
                ILMSettingsLabel,
                KeepLauncherOpen,
                MoreOptionsTitleLabel,
                MusicCheckBox,
                OpenButton,
                OptimizeButton,
                PMCheckBox,
                RunClientAsAdminCheckBox,
                SettingsTitleLabel,
                SkipSplashCheckBox,
                SECheckBox,
                TREFixButton,
                UICheckBox
            };

            foreach (Control control in ControlsToUpdate)
                control.Font = CustomFontService.CustomFontFont(control);

            Control[] Controls = {
                AnimationsCheckBox,
                GraphicsCheckBox,
                MusicCheckBox,
                PMCheckBox,
                SECheckBox,
                UICheckBox,
                DisableCutScreensCheckBox,
                RunClientAsAdminCheckBox,
                SkipSplashCheckBox,
                KeepLauncherOpen,
                EnableFXCheckBox,
                GameDirOutput
            };

            for (int i = 0; i < Controls.Length; i++)
            {
                if (Controls[i] is CheckBox)
                    ((CheckBox)Controls[i]).Checked = ConfigManager.ConfigValueIsTrue(i);
                else if (Controls[i] is RadioButton)
                    ((RadioButton)Controls[i]).Checked = ConfigManager.ConfigValueIsTrue(i);
                else if (Controls[i] is TextBox)
                    ((TextBox)Controls[i]).Text = ConfigManager.GetGameDirectory();
            }

            GameDirectoryLabel.Location = new Point((DirectoryPanel.Width - GameDirectoryLabel.Width) / 2, GameDirectoryLabel.Location.Y);
            ILMSettingsLabel.Location = new Point((ILMSettingsPanel.Width - ILMSettingsLabel.Width) / 2, ILMSettingsLabel.Location.Y);
            SettingsTitleLabel.Location = new Point((GameAndLauncherSettingsPanel.Width - SettingsTitleLabel.Width) / 2, SettingsTitleLabel.Location.Y);
            MoreOptionsTitleLabel.Location = new Point((MoreOptionsPanel.Width - MoreOptionsTitleLabel.Width) / 2, MoreOptionsTitleLabel.Location.Y);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        private void PromptGameDirChange()
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                fbd.SelectedPath = Directory.Exists(GameDirOutput.Text) ? GameDirOutput.Text : Application.StartupPath;
                fbd.Description = "Please select the path to your Vision of Hope game folder.";

                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    GameDirOutput.Text = fbd.SelectedPath;
                    ConfigManager.ChangeConfig("gameDirectory", GameDirOutput.Text);
                    ((LauncherMain)Parent).CheckForUpdates();
                }
            }
        }

        public void SetupReadyToPlayUI()
        {
            OptimizeButton.Enabled = true;
            GameDirOutput.ReadOnly = false;
            ChangeGameDirButton.Enabled = true;
        }

        public void SetupNeedsPatchingUI()
        {
            OptimizeButton.Enabled = false;
            GameDirOutput.ReadOnly = true;
            ChangeGameDirButton.Enabled = false;
        }

        public void Save()
        {
            Control[] Controls = {
                AnimationsCheckBox,
                GraphicsCheckBox,
                MusicCheckBox,
                PMCheckBox,
                SECheckBox,
                UICheckBox,
                DisableCutScreensCheckBox,
                RunClientAsAdminCheckBox,
                SkipSplashCheckBox,
                KeepLauncherOpen,
                EnableFXCheckBox,
                GameDirOutput
            };

            string[] Values = new string[Controls.Length];

            for (int i = 0; i < Controls.Length; i++)
            {
                if (Controls[i] is CheckBox)
                    Values[i] = (((CheckBox)Controls[i]).Checked ? "1" : "0");
                else if (Controls[i] is RadioButton)
                    Values[i] = (((RadioButton)Controls[i]).Checked ? "1" : "0");
                else if (Controls[i] is TextBox)
                    Values[i] = ((TextBox)Controls[i]).Text;
            }

            ConfigManager.ChangeConfig(Values);

            // ILM Changes

            if (!Directory.Exists(GameDirOutput.Text) || !File.Exists(GameDirOutput.Text + @"\live.cfg"))
                return;

            string updatedLive;

            using (StreamReader reader = new StreamReader(GameDirOutput.Text + @"\live.cfg"))
                updatedLive = reader.ReadToEnd();

            updatedLive = updatedLive.Replace("#", string.Empty);
            updatedLive = updatedLive.Replace("searchTree_00_15=", (AnimationsCheckBox.Checked ? string.Empty : "#") + "searchTree_00_15=");
            updatedLive = updatedLive.Replace("searchTree_00_14=", (GraphicsCheckBox.Checked ? string.Empty : "#") + "searchTree_00_14=");
            updatedLive = updatedLive.Replace("searchTree_00_13=", (GraphicsCheckBox.Checked ? string.Empty : "#") + "searchTree_00_13=");
            updatedLive = updatedLive.Replace("searchTree_00_12=", (MusicCheckBox.Checked ? string.Empty : "#") + "searchTree_00_12=");
            updatedLive = updatedLive.Replace("searchTree_00_11=", (PMCheckBox.Checked ? string.Empty : "#") + "searchTree_00_11=");
            updatedLive = updatedLive.Replace("searchTree_00_10=", (SECheckBox.Checked ? string.Empty : "#") + "searchTree_00_10=");
            updatedLive = updatedLive.Replace("searchTree_00_9=", (UICheckBox.Checked ? string.Empty : "#") + "searchTree_00_9=");

            try
            {
                File.WriteAllText(GameDirOutput.Text + @"\live.cfg", updatedLive);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Writing ILM Changes: " + ex.Message);
            }
        }

        private void OptimizeButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);

            if (!Directory.Exists(GameDirOutput.Text))
                return;

            string[] filesInGameDir = Directory.GetFiles(GameDirOutput.Text);
            string[] foldersInGameDir = Directory.GetDirectories(GameDirOutput.Text);

            List<string> filesToDelete = new List<string>();
            List<string> foldersToDelete = new List<string>();
            List<long> fileSizes = new List<long>();

            for (int i = 0; i < filesInGameDir.Length; i++)
            {
                if (!PatchingService.FileIsValidGameFile(Path.GetFileName(filesInGameDir[i])))
                {
                    filesToDelete.Add(filesInGameDir[i]);
                    fileSizes.Add(new FileInfo(filesInGameDir[i]).Length / 1024);
                }
            }

            for (int i = 0; i < foldersInGameDir.Length; i++)
            {
                if (!PatchingService.FolderIsValidGameFolder(Path.GetFileName(foldersInGameDir[i])))
                    foldersToDelete.Add(foldersInGameDir[i]);
            }

            if (filesToDelete.Count == 0 && foldersToDelete.Count == 0)
                MessageBox.Show("Your directory is already optimized and ready to play!", "Game Directory Already Optimized");
            else
                new OptimizeDialog(filesToDelete, fileSizes, foldersToDelete, this).ShowDialog(this);
        }

        private void ToolsButton_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
            Parent.Controls.Add(new Tools(this));
        }

        private void ChangeGameDirButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
            PromptGameDirChange();
        }

        private void EnableFXCheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void KeepLauncherOpen_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void SkipSplashCheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void DisableCutScreensCheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void RunClientAsAdminCheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void EnableFXCheckBox_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(EnableFXCheckBox);
        }

        private void EnableFXCheckBox_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(EnableFXCheckBox);
        }

        private void KeepLauncherOpen_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(KeepLauncherOpen);
        }

        private void KeepLauncherOpen_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(KeepLauncherOpen);
        }

        private void SkipSplashCheckBox_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(SkipSplashCheckBox);
        }

        private void SkipSplashCheckBox_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(SkipSplashCheckBox);
        }

        private void DisableCutScreensCheckBox_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(DisableCutScreensCheckBox);
        }

        private void DisableCutScreensCheckBox_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(DisableCutScreensCheckBox);
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(GameDirOutput.Text))
                Process.Start(GameDirOutput.Text);
            SoundService.PlaySound("Sound_Click", this);
        }

        private void staffButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);

            if (!Directory.Exists(GameDirOutput.Text))
                return;

            if (File.Exists(GameDirOutput.Text + @"\UserConfigEditor.exe"))
                Process.Start(GameDirOutput.Text + @"\UserConfigEditor.exe");
            else
            {
                DialogResult dr = MessageBox.Show("You are missing UserConfigEditor.exe from your game directory. Would you like to download it now?", "Download UserConfigEditor.exe", MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                    Process.Start(Program.PATCH_URL + "tools/UserConfigEditor.exe");
            }
        }

        private void GameDirOutput_TextChanged(object sender, EventArgs e)
        {
            ((LauncherMain)Parent.Parent).CheckForUpdates();
        }

        private void MultipleInstancesCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            WriteCfgs.WriteLogin(this);
        }

        private void newsBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
            Process.Start(e.Url.ToString());
            e.Cancel = true;
        }

        public new void MouseEnter(Control control)
        {
            control.ForeColor = Color.FromArgb(222, 69, 69);
        }

        public new void MouseLeave(Control control)
        {
            control.ForeColor = Color.White;
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void IndominusRadioButton_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void AllegianceRadioButton_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void RunClientAsAdminCheckBox_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(RunClientAsAdminCheckBox);
        }

        private void RunClientAsAdminCheckBox_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(RunClientAsAdminCheckBox);
        }

        private void BackLabel_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
            Save();
            ((LauncherMain)Parent).MoveButtonsBack();
            Dispose();
        }

        private void SidePanel_Load(object sender, EventArgs e)
        {
            Location = new Point(0, 0);
            ((LauncherMain)Parent).BringButtonsToFront(this);
            BringToFront();
        }

        private void SidePanel_MouseDown(object sender, MouseEventArgs e)
        {
            MovementService.SetMouseDownPoint(e);
        }

        private void SidePanel_MouseMove(object sender, MouseEventArgs e)
        {
            MovementService.MouseMove(this, e);
        }

        private void AnimationsCheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void GraphicsCheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void MusicCheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void PlanetMapsCheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void SoundEffectsCheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void UserInterfaceCheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void SECheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void PMCheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void UICheckBox_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);
        }

        private void AnimationsCheckBox_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(AnimationsCheckBox);
        }

        private void AnimationsCheckBox_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(AnimationsCheckBox);
        }

        private void GraphicsCheckBox_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(GraphicsCheckBox);
        }

        private void GraphicsCheckBox_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(GraphicsCheckBox);
        }

        private void SECheckBox_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(SECheckBox);
        }

        private void SECheckBox_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(SECheckBox);
        }

        private void MusicCheckBox_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(MusicCheckBox);
        }

        private void MusicCheckBox_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(MusicCheckBox);
        }

        private void PMCheckBox_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(PMCheckBox);
        }

        private void PMCheckBox_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(PMCheckBox);
        }

        private void UICheckBox_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(UICheckBox);
        }

        private void UICheckBox_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(UICheckBox);
        }

        private void BackLabel_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(BackLabel);
        }

        private void BackLabel_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(BackLabel);
        }


        private void ClientSetupButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("Sound_Click", this);

            if (!Directory.Exists(GameDirOutput.Text))
            {
                MessageBox.Show("Please configure your game directory.", "No Game Directory");
                return;
            }

            LaunchGameService.CreateShortcut("Setup");
            Process.Start(Application.StartupPath + @"\SWG Vision of Hope Setup.lnk");
        }

        private void TREFixButton_Click(object sender, EventArgs e)
        {
            if (!File.Exists(ConfigManager.GetGameDirectory() + @"\TREFix.exe"))
            {
                MessageBox.Show("TREFix.exe is missing.", "No TREFix.exe");
                return;
            }
            SoundService.PlaySound("Sound_Click", this);
            Process.Start(ConfigManager.GetGameDirectory() + @"\TREFix.exe");
        }

        private void ClientSetupButton_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(ClientSetupButton);
        }

        private void ClientSetupButton_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(ClientSetupButton);
        }

        private void TREFixButton_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(TREFixButton);
        }

        private void TREFixButton_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(TREFixButton);
        }

        private void OpenButton_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(OpenButton);
        }

        private void OpenButton_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(OpenButton);
        }

        private void ChangeGameDirButton_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(ChangeGameDirButton);
        }

        private void ChangeGameDirButton_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(ChangeGameDirButton);
        }

        private void OptimizeButton_MouseEnter(object sender, EventArgs e)
        {
            MouseEnter(OptimizeButton);
        }

        private void OptimizeButton_MouseLeave(object sender, EventArgs e)
        {
            MouseLeave(OptimizeButton);
        }
    }
}
