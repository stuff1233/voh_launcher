﻿namespace SWGVOHLauncher.forms.panel
{
    partial class SidePanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GameAndLauncherSettingsPanel = new System.Windows.Forms.Panel();
            this.SettingsTitleLabel = new System.Windows.Forms.Label();
            this.RunClientAsAdminCheckBox = new System.Windows.Forms.CheckBox();
            this.SkipSplashCheckBox = new System.Windows.Forms.CheckBox();
            this.DisableCutScreensCheckBox = new System.Windows.Forms.CheckBox();
            this.EnableFXCheckBox = new System.Windows.Forms.CheckBox();
            this.KeepLauncherOpen = new System.Windows.Forms.CheckBox();
            this.DirectoryPanel = new System.Windows.Forms.Panel();
            this.OptimizeButton = new System.Windows.Forms.Button();
            this.GameDirectoryLabel = new System.Windows.Forms.Label();
            this.OpenButton = new System.Windows.Forms.Button();
            this.GameDirOutput = new System.Windows.Forms.TextBox();
            this.ChangeGameDirButton = new System.Windows.Forms.Button();
            this.gameDirectoryTitleLabel = new System.Windows.Forms.Label();
            this.BackLabel = new System.Windows.Forms.Label();
            this.ILMSettingsPanel = new System.Windows.Forms.Panel();
            this.ILMSettingsLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.UICheckBox = new System.Windows.Forms.CheckBox();
            this.PMCheckBox = new System.Windows.Forms.CheckBox();
            this.SECheckBox = new System.Windows.Forms.CheckBox();
            this.AnimationsCheckBox = new System.Windows.Forms.CheckBox();
            this.GraphicsCheckBox = new System.Windows.Forms.CheckBox();
            this.MusicCheckBox = new System.Windows.Forms.CheckBox();
            this.MoreOptionsPanel = new System.Windows.Forms.Panel();
            this.TREFixButton = new System.Windows.Forms.Button();
            this.ClientSetupButton = new System.Windows.Forms.Button();
            this.MoreOptionsTitleLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GameAndLauncherSettingsPanel.SuspendLayout();
            this.DirectoryPanel.SuspendLayout();
            this.ILMSettingsPanel.SuspendLayout();
            this.MoreOptionsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // GameAndLauncherSettingsPanel
            // 
            this.GameAndLauncherSettingsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.GameAndLauncherSettingsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GameAndLauncherSettingsPanel.Controls.Add(this.SettingsTitleLabel);
            this.GameAndLauncherSettingsPanel.Controls.Add(this.RunClientAsAdminCheckBox);
            this.GameAndLauncherSettingsPanel.Controls.Add(this.SkipSplashCheckBox);
            this.GameAndLauncherSettingsPanel.Controls.Add(this.DisableCutScreensCheckBox);
            this.GameAndLauncherSettingsPanel.Controls.Add(this.EnableFXCheckBox);
            this.GameAndLauncherSettingsPanel.Controls.Add(this.KeepLauncherOpen);
            this.GameAndLauncherSettingsPanel.Location = new System.Drawing.Point(553, 195);
            this.GameAndLauncherSettingsPanel.Name = "GameAndLauncherSettingsPanel";
            this.GameAndLauncherSettingsPanel.Size = new System.Drawing.Size(380, 180);
            this.GameAndLauncherSettingsPanel.TabIndex = 43;
            // 
            // SettingsTitleLabel
            // 
            this.SettingsTitleLabel.AutoSize = true;
            this.SettingsTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.SettingsTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SettingsTitleLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.SettingsTitleLabel.Location = new System.Drawing.Point(67, 2);
            this.SettingsTitleLabel.Name = "SettingsTitleLabel";
            this.SettingsTitleLabel.Size = new System.Drawing.Size(244, 31);
            this.SettingsTitleLabel.TabIndex = 53;
            this.SettingsTitleLabel.Text = "game and launcher";
            // 
            // RunClientAsAdminCheckBox
            // 
            this.RunClientAsAdminCheckBox.AutoSize = true;
            this.RunClientAsAdminCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.RunClientAsAdminCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RunClientAsAdminCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.RunClientAsAdminCheckBox.Location = new System.Drawing.Point(13, 88);
            this.RunClientAsAdminCheckBox.Name = "RunClientAsAdminCheckBox";
            this.RunClientAsAdminCheckBox.Size = new System.Drawing.Size(305, 33);
            this.RunClientAsAdminCheckBox.TabIndex = 52;
            this.RunClientAsAdminCheckBox.Text = "run client as administrator";
            this.RunClientAsAdminCheckBox.UseVisualStyleBackColor = false;
            this.RunClientAsAdminCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RunClientAsAdminCheckBox_MouseClick);
            this.RunClientAsAdminCheckBox.MouseEnter += new System.EventHandler(this.RunClientAsAdminCheckBox_MouseEnter);
            this.RunClientAsAdminCheckBox.MouseLeave += new System.EventHandler(this.RunClientAsAdminCheckBox_MouseLeave);
            // 
            // SkipSplashCheckBox
            // 
            this.SkipSplashCheckBox.AutoSize = true;
            this.SkipSplashCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.SkipSplashCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SkipSplashCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.SkipSplashCheckBox.Location = new System.Drawing.Point(13, 60);
            this.SkipSplashCheckBox.Name = "SkipSplashCheckBox";
            this.SkipSplashCheckBox.Size = new System.Drawing.Size(298, 33);
            this.SkipSplashCheckBox.TabIndex = 49;
            this.SkipSplashCheckBox.Text = "skip introduction screens";
            this.SkipSplashCheckBox.UseVisualStyleBackColor = false;
            this.SkipSplashCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SkipSplashCheckBox_MouseClick);
            this.SkipSplashCheckBox.MouseEnter += new System.EventHandler(this.SkipSplashCheckBox_MouseEnter);
            this.SkipSplashCheckBox.MouseLeave += new System.EventHandler(this.SkipSplashCheckBox_MouseLeave);
            // 
            // DisableCutScreensCheckBox
            // 
            this.DisableCutScreensCheckBox.AutoSize = true;
            this.DisableCutScreensCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.DisableCutScreensCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DisableCutScreensCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.DisableCutScreensCheckBox.Location = new System.Drawing.Point(13, 32);
            this.DisableCutScreensCheckBox.Name = "DisableCutScreensCheckBox";
            this.DisableCutScreensCheckBox.Size = new System.Drawing.Size(231, 33);
            this.DisableCutScreensCheckBox.TabIndex = 48;
            this.DisableCutScreensCheckBox.Text = "disable cut scenes";
            this.DisableCutScreensCheckBox.UseVisualStyleBackColor = false;
            this.DisableCutScreensCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DisableCutScreensCheckBox_MouseClick);
            this.DisableCutScreensCheckBox.MouseEnter += new System.EventHandler(this.DisableCutScreensCheckBox_MouseEnter);
            this.DisableCutScreensCheckBox.MouseLeave += new System.EventHandler(this.DisableCutScreensCheckBox_MouseLeave);
            // 
            // EnableFXCheckBox
            // 
            this.EnableFXCheckBox.AutoSize = true;
            this.EnableFXCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.EnableFXCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EnableFXCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.EnableFXCheckBox.Location = new System.Drawing.Point(13, 144);
            this.EnableFXCheckBox.Name = "EnableFXCheckBox";
            this.EnableFXCheckBox.Size = new System.Drawing.Size(352, 33);
            this.EnableFXCheckBox.TabIndex = 47;
            this.EnableFXCheckBox.Text = "enable launcher sound effects";
            this.EnableFXCheckBox.UseVisualStyleBackColor = false;
            this.EnableFXCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.EnableFXCheckBox_MouseClick);
            this.EnableFXCheckBox.MouseEnter += new System.EventHandler(this.EnableFXCheckBox_MouseEnter);
            this.EnableFXCheckBox.MouseLeave += new System.EventHandler(this.EnableFXCheckBox_MouseLeave);
            // 
            // KeepLauncherOpen
            // 
            this.KeepLauncherOpen.AutoSize = true;
            this.KeepLauncherOpen.BackColor = System.Drawing.Color.Transparent;
            this.KeepLauncherOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeepLauncherOpen.ForeColor = System.Drawing.SystemColors.Control;
            this.KeepLauncherOpen.Location = new System.Drawing.Point(13, 116);
            this.KeepLauncherOpen.Name = "KeepLauncherOpen";
            this.KeepLauncherOpen.Size = new System.Drawing.Size(348, 33);
            this.KeepLauncherOpen.TabIndex = 46;
            this.KeepLauncherOpen.Text = "keep launcher open after play";
            this.KeepLauncherOpen.UseVisualStyleBackColor = false;
            this.KeepLauncherOpen.MouseClick += new System.Windows.Forms.MouseEventHandler(this.KeepLauncherOpen_MouseClick);
            this.KeepLauncherOpen.MouseEnter += new System.EventHandler(this.KeepLauncherOpen_MouseEnter);
            this.KeepLauncherOpen.MouseLeave += new System.EventHandler(this.KeepLauncherOpen_MouseLeave);
            // 
            // DirectoryPanel
            // 
            this.DirectoryPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.DirectoryPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DirectoryPanel.Controls.Add(this.OptimizeButton);
            this.DirectoryPanel.Controls.Add(this.GameDirectoryLabel);
            this.DirectoryPanel.Controls.Add(this.OpenButton);
            this.DirectoryPanel.Controls.Add(this.GameDirOutput);
            this.DirectoryPanel.Controls.Add(this.ChangeGameDirButton);
            this.DirectoryPanel.Controls.Add(this.gameDirectoryTitleLabel);
            this.DirectoryPanel.Location = new System.Drawing.Point(167, 195);
            this.DirectoryPanel.Name = "DirectoryPanel";
            this.DirectoryPanel.Size = new System.Drawing.Size(380, 126);
            this.DirectoryPanel.TabIndex = 44;
            // 
            // OptimizeButton
            // 
            this.OptimizeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.OptimizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OptimizeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptimizeButton.ForeColor = System.Drawing.SystemColors.Control;
            this.OptimizeButton.Location = new System.Drawing.Point(243, 74);
            this.OptimizeButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.OptimizeButton.Name = "OptimizeButton";
            this.OptimizeButton.Size = new System.Drawing.Size(120, 40);
            this.OptimizeButton.TabIndex = 49;
            this.OptimizeButton.Text = "optimize";
            this.OptimizeButton.UseVisualStyleBackColor = false;
            this.OptimizeButton.Click += new System.EventHandler(this.OptimizeButton_Click);
            this.OptimizeButton.MouseEnter += new System.EventHandler(this.OptimizeButton_MouseEnter);
            this.OptimizeButton.MouseLeave += new System.EventHandler(this.OptimizeButton_MouseLeave);
            // 
            // GameDirectoryLabel
            // 
            this.GameDirectoryLabel.AutoSize = true;
            this.GameDirectoryLabel.BackColor = System.Drawing.Color.Transparent;
            this.GameDirectoryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GameDirectoryLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.GameDirectoryLabel.Location = new System.Drawing.Point(93, 2);
            this.GameDirectoryLabel.Name = "GameDirectoryLabel";
            this.GameDirectoryLabel.Size = new System.Drawing.Size(193, 31);
            this.GameDirectoryLabel.TabIndex = 48;
            this.GameDirectoryLabel.Text = "game directory";
            // 
            // OpenButton
            // 
            this.OpenButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.OpenButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OpenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenButton.ForeColor = System.Drawing.SystemColors.Control;
            this.OpenButton.Location = new System.Drawing.Point(15, 74);
            this.OpenButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(110, 40);
            this.OpenButton.TabIndex = 47;
            this.OpenButton.Text = "open";
            this.OpenButton.UseVisualStyleBackColor = false;
            this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            this.OpenButton.MouseEnter += new System.EventHandler(this.OpenButton_MouseEnter);
            this.OpenButton.MouseLeave += new System.EventHandler(this.OpenButton_MouseLeave);
            // 
            // GameDirOutput
            // 
            this.GameDirOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.GameDirOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GameDirOutput.ForeColor = System.Drawing.SystemColors.Control;
            this.GameDirOutput.Location = new System.Drawing.Point(15, 36);
            this.GameDirOutput.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.GameDirOutput.Name = "GameDirOutput";
            this.GameDirOutput.ReadOnly = true;
            this.GameDirOutput.Size = new System.Drawing.Size(348, 31);
            this.GameDirOutput.TabIndex = 45;
            // 
            // ChangeGameDirButton
            // 
            this.ChangeGameDirButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.ChangeGameDirButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeGameDirButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChangeGameDirButton.ForeColor = System.Drawing.SystemColors.Control;
            this.ChangeGameDirButton.Location = new System.Drawing.Point(129, 74);
            this.ChangeGameDirButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ChangeGameDirButton.Name = "ChangeGameDirButton";
            this.ChangeGameDirButton.Size = new System.Drawing.Size(110, 40);
            this.ChangeGameDirButton.TabIndex = 46;
            this.ChangeGameDirButton.Text = "change";
            this.ChangeGameDirButton.UseVisualStyleBackColor = false;
            this.ChangeGameDirButton.Click += new System.EventHandler(this.ChangeGameDirButton_Click);
            this.ChangeGameDirButton.MouseEnter += new System.EventHandler(this.ChangeGameDirButton_MouseEnter);
            this.ChangeGameDirButton.MouseLeave += new System.EventHandler(this.ChangeGameDirButton_MouseLeave);
            // 
            // gameDirectoryTitleLabel
            // 
            this.gameDirectoryTitleLabel.AutoSize = true;
            this.gameDirectoryTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.gameDirectoryTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameDirectoryTitleLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.gameDirectoryTitleLabel.Location = new System.Drawing.Point(-16, -5);
            this.gameDirectoryTitleLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.gameDirectoryTitleLabel.Name = "gameDirectoryTitleLabel";
            this.gameDirectoryTitleLabel.Size = new System.Drawing.Size(0, 24);
            this.gameDirectoryTitleLabel.TabIndex = 44;
            // 
            // BackLabel
            // 
            this.BackLabel.AutoSize = true;
            this.BackLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.BackLabel.Location = new System.Drawing.Point(496, 472);
            this.BackLabel.Name = "BackLabel";
            this.BackLabel.Size = new System.Drawing.Size(109, 31);
            this.BackLabel.TabIndex = 46;
            this.BackLabel.Text = "← Back";
            this.BackLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.BackLabel_MouseClick);
            this.BackLabel.MouseEnter += new System.EventHandler(this.BackLabel_MouseEnter);
            this.BackLabel.MouseLeave += new System.EventHandler(this.BackLabel_MouseLeave);
            // 
            // ILMSettingsPanel
            // 
            this.ILMSettingsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.ILMSettingsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ILMSettingsPanel.Controls.Add(this.ILMSettingsLabel);
            this.ILMSettingsPanel.Controls.Add(this.label4);
            this.ILMSettingsPanel.Controls.Add(this.UICheckBox);
            this.ILMSettingsPanel.Controls.Add(this.PMCheckBox);
            this.ILMSettingsPanel.Controls.Add(this.SECheckBox);
            this.ILMSettingsPanel.Controls.Add(this.AnimationsCheckBox);
            this.ILMSettingsPanel.Controls.Add(this.GraphicsCheckBox);
            this.ILMSettingsPanel.Controls.Add(this.MusicCheckBox);
            this.ILMSettingsPanel.Location = new System.Drawing.Point(167, 327);
            this.ILMSettingsPanel.Name = "ILMSettingsPanel";
            this.ILMSettingsPanel.Size = new System.Drawing.Size(380, 135);
            this.ILMSettingsPanel.TabIndex = 47;
            // 
            // ILMSettingsLabel
            // 
            this.ILMSettingsLabel.AutoSize = true;
            this.ILMSettingsLabel.BackColor = System.Drawing.Color.Transparent;
            this.ILMSettingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ILMSettingsLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.ILMSettingsLabel.Location = new System.Drawing.Point(165, 2);
            this.ILMSettingsLabel.Name = "ILMSettingsLabel";
            this.ILMSettingsLabel.Size = new System.Drawing.Size(48, 31);
            this.ILMSettingsLabel.TabIndex = 48;
            this.ILMSettingsLabel.Text = "ilm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(-16, -5);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 24);
            this.label4.TabIndex = 44;
            // 
            // UICheckBox
            // 
            this.UICheckBox.AutoSize = true;
            this.UICheckBox.BackColor = System.Drawing.Color.Transparent;
            this.UICheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UICheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.UICheckBox.Location = new System.Drawing.Point(192, 99);
            this.UICheckBox.Name = "UICheckBox";
            this.UICheckBox.Size = new System.Drawing.Size(181, 33);
            this.UICheckBox.TabIndex = 57;
            this.UICheckBox.Text = "User Interface";
            this.UICheckBox.UseVisualStyleBackColor = false;
            this.UICheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.UICheckBox_MouseClick);
            this.UICheckBox.MouseEnter += new System.EventHandler(this.UICheckBox_MouseEnter);
            this.UICheckBox.MouseLeave += new System.EventHandler(this.UICheckBox_MouseLeave);
            // 
            // PMCheckBox
            // 
            this.PMCheckBox.AutoSize = true;
            this.PMCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.PMCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PMCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.PMCheckBox.Location = new System.Drawing.Point(192, 66);
            this.PMCheckBox.Name = "PMCheckBox";
            this.PMCheckBox.Size = new System.Drawing.Size(165, 33);
            this.PMCheckBox.TabIndex = 59;
            this.PMCheckBox.Text = "Planet Maps";
            this.PMCheckBox.UseVisualStyleBackColor = false;
            this.PMCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PMCheckBox_MouseClick);
            this.PMCheckBox.MouseEnter += new System.EventHandler(this.PMCheckBox_MouseEnter);
            this.PMCheckBox.MouseLeave += new System.EventHandler(this.PMCheckBox_MouseLeave);
            // 
            // SECheckBox
            // 
            this.SECheckBox.AutoSize = true;
            this.SECheckBox.BackColor = System.Drawing.Color.Transparent;
            this.SECheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SECheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.SECheckBox.Location = new System.Drawing.Point(6, 99);
            this.SECheckBox.Name = "SECheckBox";
            this.SECheckBox.Size = new System.Drawing.Size(180, 33);
            this.SECheckBox.TabIndex = 56;
            this.SECheckBox.Text = "Sound Effects";
            this.SECheckBox.UseVisualStyleBackColor = false;
            this.SECheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SECheckBox_MouseClick);
            this.SECheckBox.MouseEnter += new System.EventHandler(this.SECheckBox_MouseEnter);
            this.SECheckBox.MouseLeave += new System.EventHandler(this.SECheckBox_MouseLeave);
            // 
            // AnimationsCheckBox
            // 
            this.AnimationsCheckBox.AutoSize = true;
            this.AnimationsCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.AnimationsCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AnimationsCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.AnimationsCheckBox.Location = new System.Drawing.Point(6, 66);
            this.AnimationsCheckBox.Name = "AnimationsCheckBox";
            this.AnimationsCheckBox.Size = new System.Drawing.Size(150, 33);
            this.AnimationsCheckBox.TabIndex = 58;
            this.AnimationsCheckBox.Text = "Animations";
            this.AnimationsCheckBox.UseVisualStyleBackColor = false;
            this.AnimationsCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.AnimationsCheckBox_MouseClick);
            this.AnimationsCheckBox.MouseEnter += new System.EventHandler(this.AnimationsCheckBox_MouseEnter);
            this.AnimationsCheckBox.MouseLeave += new System.EventHandler(this.AnimationsCheckBox_MouseLeave);
            // 
            // GraphicsCheckBox
            // 
            this.GraphicsCheckBox.AutoSize = true;
            this.GraphicsCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.GraphicsCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GraphicsCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.GraphicsCheckBox.Location = new System.Drawing.Point(6, 33);
            this.GraphicsCheckBox.Name = "GraphicsCheckBox";
            this.GraphicsCheckBox.Size = new System.Drawing.Size(128, 33);
            this.GraphicsCheckBox.TabIndex = 54;
            this.GraphicsCheckBox.Text = "Graphics";
            this.GraphicsCheckBox.UseVisualStyleBackColor = false;
            this.GraphicsCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GraphicsCheckBox_MouseClick);
            this.GraphicsCheckBox.MouseEnter += new System.EventHandler(this.GraphicsCheckBox_MouseEnter);
            this.GraphicsCheckBox.MouseLeave += new System.EventHandler(this.GraphicsCheckBox_MouseLeave);
            // 
            // MusicCheckBox
            // 
            this.MusicCheckBox.AutoSize = true;
            this.MusicCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.MusicCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MusicCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.MusicCheckBox.Location = new System.Drawing.Point(192, 33);
            this.MusicCheckBox.Name = "MusicCheckBox";
            this.MusicCheckBox.Size = new System.Drawing.Size(95, 33);
            this.MusicCheckBox.TabIndex = 55;
            this.MusicCheckBox.Text = "Music";
            this.MusicCheckBox.UseVisualStyleBackColor = false;
            this.MusicCheckBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MusicCheckBox_MouseClick);
            this.MusicCheckBox.MouseEnter += new System.EventHandler(this.MusicCheckBox_MouseEnter);
            this.MusicCheckBox.MouseLeave += new System.EventHandler(this.MusicCheckBox_MouseLeave);
            // 
            // MoreOptionsPanel
            // 
            this.MoreOptionsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.MoreOptionsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MoreOptionsPanel.Controls.Add(this.TREFixButton);
            this.MoreOptionsPanel.Controls.Add(this.ClientSetupButton);
            this.MoreOptionsPanel.Controls.Add(this.MoreOptionsTitleLabel);
            this.MoreOptionsPanel.Controls.Add(this.label2);
            this.MoreOptionsPanel.Location = new System.Drawing.Point(553, 381);
            this.MoreOptionsPanel.Name = "MoreOptionsPanel";
            this.MoreOptionsPanel.Size = new System.Drawing.Size(380, 81);
            this.MoreOptionsPanel.TabIndex = 49;
            // 
            // TREFixButton
            // 
            this.TREFixButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.TREFixButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TREFixButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TREFixButton.ForeColor = System.Drawing.SystemColors.Control;
            this.TREFixButton.Location = new System.Drawing.Point(191, 35);
            this.TREFixButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.TREFixButton.Name = "TREFixButton";
            this.TREFixButton.Size = new System.Drawing.Size(125, 40);
            this.TREFixButton.TabIndex = 50;
            this.TREFixButton.Text = "tre fix";
            this.TREFixButton.UseVisualStyleBackColor = false;
            this.TREFixButton.Click += new System.EventHandler(this.TREFixButton_Click);
            this.TREFixButton.MouseEnter += new System.EventHandler(this.TREFixButton_MouseEnter);
            this.TREFixButton.MouseLeave += new System.EventHandler(this.TREFixButton_MouseLeave);
            // 
            // ClientSetupButton
            // 
            this.ClientSetupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.ClientSetupButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClientSetupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientSetupButton.ForeColor = System.Drawing.SystemColors.Control;
            this.ClientSetupButton.Location = new System.Drawing.Point(62, 35);
            this.ClientSetupButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ClientSetupButton.Name = "ClientSetupButton";
            this.ClientSetupButton.Size = new System.Drawing.Size(125, 40);
            this.ClientSetupButton.TabIndex = 49;
            this.ClientSetupButton.Text = "client setup";
            this.ClientSetupButton.UseVisualStyleBackColor = false;
            this.ClientSetupButton.Click += new System.EventHandler(this.ClientSetupButton_Click);
            this.ClientSetupButton.MouseEnter += new System.EventHandler(this.ClientSetupButton_MouseEnter);
            this.ClientSetupButton.MouseLeave += new System.EventHandler(this.ClientSetupButton_MouseLeave);
            // 
            // MoreOptionsTitleLabel
            // 
            this.MoreOptionsTitleLabel.AutoSize = true;
            this.MoreOptionsTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.MoreOptionsTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MoreOptionsTitleLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.MoreOptionsTitleLabel.Location = new System.Drawing.Point(104, 2);
            this.MoreOptionsTitleLabel.Name = "MoreOptionsTitleLabel";
            this.MoreOptionsTitleLabel.Size = new System.Drawing.Size(170, 31);
            this.MoreOptionsTitleLabel.TabIndex = 48;
            this.MoreOptionsTitleLabel.Text = "more options";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(-16, -5);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 24);
            this.label2.TabIndex = 44;
            // 
            // SidePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.MoreOptionsPanel);
            this.Controls.Add(this.ILMSettingsPanel);
            this.Controls.Add(this.BackLabel);
            this.Controls.Add(this.GameAndLauncherSettingsPanel);
            this.Controls.Add(this.DirectoryPanel);
            this.DoubleBuffered = true;
            this.Name = "SidePanel";
            this.Size = new System.Drawing.Size(1100, 620);
            this.Load += new System.EventHandler(this.SidePanel_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SidePanel_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SidePanel_MouseMove);
            this.GameAndLauncherSettingsPanel.ResumeLayout(false);
            this.GameAndLauncherSettingsPanel.PerformLayout();
            this.DirectoryPanel.ResumeLayout(false);
            this.DirectoryPanel.PerformLayout();
            this.ILMSettingsPanel.ResumeLayout(false);
            this.ILMSettingsPanel.PerformLayout();
            this.MoreOptionsPanel.ResumeLayout(false);
            this.MoreOptionsPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel GameAndLauncherSettingsPanel;
        private System.Windows.Forms.CheckBox RunClientAsAdminCheckBox;
        private System.Windows.Forms.CheckBox SkipSplashCheckBox;
        private System.Windows.Forms.CheckBox DisableCutScreensCheckBox;
        private System.Windows.Forms.CheckBox EnableFXCheckBox;
        private System.Windows.Forms.CheckBox KeepLauncherOpen;
        private System.Windows.Forms.Panel DirectoryPanel;
        private System.Windows.Forms.Button OptimizeButton;
        private System.Windows.Forms.Label GameDirectoryLabel;
        private System.Windows.Forms.Button OpenButton;
        private System.Windows.Forms.TextBox GameDirOutput;
        private System.Windows.Forms.Button ChangeGameDirButton;
        private System.Windows.Forms.Label gameDirectoryTitleLabel;
        private System.Windows.Forms.Label BackLabel;
        private System.Windows.Forms.Label SettingsTitleLabel;
        private System.Windows.Forms.Panel ILMSettingsPanel;
        private System.Windows.Forms.Label ILMSettingsLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox AnimationsCheckBox;
        private System.Windows.Forms.CheckBox GraphicsCheckBox;
        private System.Windows.Forms.CheckBox MusicCheckBox;
        private System.Windows.Forms.CheckBox PMCheckBox;
        private System.Windows.Forms.CheckBox SECheckBox;
        private System.Windows.Forms.CheckBox UICheckBox;
        private System.Windows.Forms.Panel MoreOptionsPanel;
        private System.Windows.Forms.Label MoreOptionsTitleLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button TREFixButton;
        private System.Windows.Forms.Button ClientSetupButton;
    }
}
