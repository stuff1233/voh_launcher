﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using SWGVOHLauncher.forms.panel;
using SWGVOHLauncher.forms.login_form;
using SWGVOHLauncher.src.services.sound;

namespace SWGVOHLauncher.forms
{
    public partial class AccountDropDown : UserControl
    {
        private readonly LauncherMain Main;

        public AccountDropDown(LauncherMain Main)
        {
            this.Main = Main;
            InitializeComponent();
        }

        private void MyAccountLabel_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", Main.GetSidePanel());
            Process.Start("https://swghope.com/forums/usercp.php");
            Dispose();
        }

        private void MyAccountLabel_MouseEnter(object sender, EventArgs e)
        {
            ((LauncherMain)Parent).GetSidePanel().MouseEnter(MyAccountLabel);
        }

        private void MyAccountLabel_MouseLeave(object sender, EventArgs e)
        {
            ((LauncherMain)Parent).GetSidePanel().MouseLeave(MyAccountLabel);
        }

        private void AccountDropDown_Load(object sender, EventArgs e)
        {
            Point LabelLocation = ((LauncherMain)Parent).GetAccountLabelLocation();
            Location = new Point(LabelLocation.X, LabelLocation.Y + 25);
            BringToFront();
        }

        private void SettingsLabel_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", Main.GetSidePanel());
            Parent.Controls.Add(new SidePanel());
            Dispose();
        }

        private void SettingsLabel_MouseEnter(object sender, EventArgs e)
        {
            ((LauncherMain)Parent).GetSidePanel().MouseEnter(SettingsLabel);
        }

        private void SettingsLabel_MouseLeave(object sender, EventArgs e)
        {
            ((LauncherMain)Parent).GetSidePanel().MouseLeave(SettingsLabel);
        }

        private void LogoutLabel_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", Main.GetSidePanel());
            Properties.Settings.Default.RememberMeUsername = null;
            Properties.Settings.Default.RememberMePassword = null;
            Properties.Settings.Default.Save();
            Parent.Controls.Add(new AccountLogin((LauncherMain)Parent));
            Dispose();
        }

        private void LogoutLabel_MouseEnter(object sender, EventArgs e)
        {
            ((LauncherMain)Parent).GetSidePanel().MouseEnter(LogoutLabel);
        }

        private void LogoutLabel_MouseLeave(object sender, EventArgs e)
        {
            ((LauncherMain)Parent).GetSidePanel().MouseLeave(LogoutLabel);
        }
    }
}
