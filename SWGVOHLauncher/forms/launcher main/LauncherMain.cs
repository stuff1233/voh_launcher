﻿/*************************************************************************
 * Copyright (C) 2017 <SWG Vision of Hope>
 *
 * This file is part of the SWG Vision of Hope Launcher.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *************************************************************************/
// @author: Iosnowore
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Net;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Collections.Generic;
using SWGVOHLauncher.forms;
using SWGVOHLauncher.forms.alert;
using SWGVOHLauncher.forms.article;
using SWGVOHLauncher.forms.login_form;
using SWGVOHLauncher.forms.panel;
using SWGVOHLauncher.src.custom;
using SWGVOHLauncher.src.services.font;
using SWGVOHLauncher.src.services.game;
using SWGVOHLauncher.src.services.movement;
using SWGVOHLauncher.src.services.patching;
using SWGVOHLauncher.src.services.sound;
using SWGVOHLauncher.src.services.Reader;
using SWGVOHLauncher.src.services.config;
using SWGVOHLauncher.forms.status;

namespace SWGVOHLauncher
{
    public partial class LauncherMain : Form
    {
        private static CustomProgressBar progressBar;
        private static PatchSpeedService patchSpeedSrv;
        private static readonly SidePanel sidePanel = new SidePanel();
        private static readonly System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private static readonly System.Windows.Forms.Timer BackgroundTimer = new System.Windows.Forms.Timer();
        private static byte BackgroundIndex = 0;
        private static string PatchUrl;
        private static Article ActiveArticle;
        private AccountDropDown AccountDropDown;

        public LauncherMain()
        {
            InitializeComponent();
            BackgroundImage = Properties.Resources.bg_launcher;
            ConfigManager.MakeLauncherConfigIfDoesntExist();
            Control[] ControlsToUpdate = {
                discordLabel,
                DonateLabel,
                playButton,
                tcgLabel,
                usernameLabel,
                websiteLabel
            };

            foreach (Control control in ControlsToUpdate)
                control.Font = CustomFontService.CustomFontFont(control);

            versionLabel.Text = "v" + Application.ProductVersion;
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        public static void SetActiveArticle(Article ActiveArticle)
        {
            LauncherMain.ActiveArticle = ActiveArticle;
        }

        public static Article GetActiveArticle()
        {
            return ActiveArticle;
        }

        public SidePanel GetSidePanel()
        {
            return sidePanel;
        }

        public void BringButtonsToFront(UserControl AccountLogin)
        {
            exitLabel.Parent = AccountLogin;
            exitLabel.BringToFront();
        }

        public void MoveButtonsBack()
        {
            exitLabel.Parent = this;
        }

        private void addProgressBar(string text)
        {
            progressBar = new CustomProgressBar();
            progressBar.DisplayStyle = ProgressBarDisplayText.CustomText;
            progressBar.SetText(text);
            progressBar.Size = new Size(1100, 25);
            progressBar.Location = new Point(0, 595);
            progressBar.MouseClick += ProgressBar_MouseClick;
            progressBar.BringToFront();
            Controls.Add(progressBar);
        }

        public bool ProgressBarIsFull()
        {
            return progressBar.Value == progressBar.Maximum;
        }

        private void ProgressBar_MouseClick(object sender, MouseEventArgs e)
        {
            int numberOfPatches = PatchingService.GetPatchesToPatchCount();

            if (numberOfPatches == 0)
                return;

            string patches = null;

            for (int i = 0; i < numberOfPatches; i++)
            {
                patches += PatchingService.GetPatchToPatch(i).GetName();

                if (i != numberOfPatches - 1)
                    patches += ", ";
            }
            MessageBox.Show(patches, "Patches to Install (" + numberOfPatches + ")");
        }

        private void DownloadPatch(string patch)
        {
            Thread.Sleep(50);
            //patchSpeedSrv.UpdatePatch(patch.ToLower().Replace('_', ' '));

            using (WebClient client = new WebClient())
            {
                client.Proxy = GlobalProxySelection.GetEmptyWebProxy();
                client.Headers.Add("User-Agent: Other");
                client.DownloadProgressChanged += Client_DownloadProgressChanged;
                client.DownloadFileCompleted += Client_DownloadFileCompleted;
                progressBar.SetText("Installing..\t" + Math.Round((((double)progressBar.Value / progressBar.Maximum) * 100), 2) + "%");
                client.DownloadFileAsync(new Uri(PatchingService.GetPatchDownloadURL(patch)), PatchingService.GetPatchLocalDir(patch, ConfigManager.GetGameDirectory()));
            }
        }

        public void UserLoggedIn()
        {
            progressBar.Dispose();
            addProgressBar(CustomProgressBar.LAUNCHER_STATES[1]);
            exitLabel.Parent = this;
            tcgLabel.Enabled = true;
            websiteLabel.Enabled = true;
            discordLabel.Enabled = true;
            DonateLabel.Enabled = true;
            usernameLabel.Visible = true;
            usernameLabel.Text = "⯆" + Program.GetUsername();
            usernameLabel.Location = new Point(exitLabel.Location.X - usernameLabel.Width - 10, exitLabel.Location.Y + 5);
            CheckForUpdates();
            BackgroundTimer.Tick += BackgroundTimer_Tick;
            BackgroundTimer.Interval = 30000;
            BackgroundTimer.Enabled = true;

            // Alert
            string Alert = WebClientReader.GetWebPageContent(Program.CFG_URL + "alert.txt");
            if (!string.IsNullOrEmpty(Alert))
            {
                string[] AlertData = Alert.Split('\t');
                Controls.Add(new Alert(AlertData[0], AlertData.Length == 2 ? AlertData[1] : null));
            }
                

            // News
            string News = WebClientReader.GetWebPageContent(Program.CFG_URL + "news.txt");
            string[] Articles = News.Split('\n');
            for (int i = 0; i < Articles.Length - 1; i++)
            {
                string[] ArticleParts = Articles[i].Split('\t');
                Controls.Add(new Article(ArticleParts[0], ArticleParts[1], ArticleParts[2], i, this));
            }

            // Latest Patch
            string Patch = WebClientReader.GetWebPageContent(Program.CFG_URL + "update.txt");
            string[] Text = Patch.Split('\t');
            LatestPatchLabel.Text = Text[0];
            PatchUrl = Text[1];

            // Server Status Widget
            for (int i = 0; i < 2; i++)
                Controls.Add(new StatusWidget(i));
        }

        private void BackgroundTimer_Tick(object sender, EventArgs e)
        {
            //BackgroundPanel.BackgroundImage = Backgrounds.Backgrounds.GetImage(Backgrounds.Backgrounds.GetNumberOfImages() <= BackgroundIndex ? 0 : ++BackgroundIndex);
        }

        private void LauncherMain_Load(object sender, EventArgs e)
        {
            addProgressBar(CustomProgressBar.LAUNCHER_STATES[2]);
            Controls.Add(new AccountLogin(this));
        }

        public void CheckForUpdates()
        {
            if (progressBar.GetText().Equals(CustomProgressBar.LAUNCHER_STATES[2]))
                return;
            else if (!Directory.Exists(ConfigManager.GetGameDirectory()))
            {
                playButton.Enabled = false;
                progressBar.Dispose();
                addProgressBar(CustomProgressBar.LAUNCHER_STATES[3]);
                return;
            }
            if (!PatchingService.GetPatchServerIsOffline())
                PatchingService.SetPatches(ConfigManager.GetGameDirectory());

            if (PatchingService.GetPatchesToPatchCount() == 0)
                SetupReadyToPlayUI();
            else
                SetupPatchButton();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (progressBar.Value < progressBar.Maximum)
                progressBar.Value++;

            //if (progressBarAll.Value < progressBarAll.Maximum)
             //   progressBarAll.Value++;

           // if (progressBar.Value == progressBar.Maximum && progressBarAll.Value == progressBarAll.Maximum)
            //    timer.Enabled = false;
        }

        private void SetupPatchButton()
        {
            playButton.Text = "Patch";
            playButton.Enabled = true;
            progressBar.Dispose();
            addProgressBar(CustomProgressBar.LAUNCHER_STATES[1]);
        }

        private void Client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            int total = (int)(PatchingService.GetPatchToPatch(0).GetBytes() / 1000);

            if (progressBar.Value + total < progressBar.Maximum)
                progressBar.Value += total;

            PatchingService.RemovePatchToPatch(0);

            if (PatchingService.GetPatchesToPatchCount() == 0)
            {
                //patchSpeedSrv.StopTimer();
                progressBar.Maximum = 100;
                progressBar.Value = 0;
                SetupReadyToPlayUI();
            }
            else
                DownloadPatch(PatchingService.GetPatchToPatch(0).GetName());
        }

        private void SetupReadyToPlayUI()
        {
            SoundService.PlaySound("Play_Ready", sidePanel);
            progressBar.SetText(CustomProgressBar.LAUNCHER_STATES[0]);
            timer.Interval = 1;
            timer.Tick += Timer_Tick;
            timer.Enabled = true;
            playButton.Text = "play";
            playButton.Enabled = true;
            sidePanel.SetupReadyToPlayUI();
        }

        private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            //patchSpeedSrv.AddIncomingBytes(e.BytesReceived);
        }

        private void LauncherMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (progressBar.GetText().StartsWith("patching"))
            {
                DialogResult dr = MessageBox.Show("Are you sure you want to exit? The launcher is in the middle of patching.", "Exit Confirmation", MessageBoxButtons.YesNo);

                if (dr == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void LauncherMain_MouseMove(object sender, MouseEventArgs e)
        {
            MovementService.MouseMove(this, e);
        }

        private void LauncherMain_MouseDown(object sender, MouseEventArgs e)
        {
            MovementService.SetMouseDownPoint(e);
        }

        private void exitPictureBox_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            if (progressBar.GetText().Equals(CustomProgressBar.LAUNCHER_STATES[0]))
            {
                SoundService.PlaySound("Sound_Play", sidePanel);
                LaunchGameService.LaunchGame(ConfigManager.GetGameDirectory(), this);

                if (!ConfigManager.KeepLauncherOpen())
                {
                    if (ConfigManager.SFXIsEnabled())
                        Thread.Sleep(4500);
                    Application.Exit();
                }
            }
            else if (progressBar.GetText().Equals(CustomProgressBar.LAUNCHER_STATES[1]))
            {
                progressBar.Dispose();
                addProgressBar("Installing..");
                progressBar.Maximum = PatchingService.GetTotalPatchSizes();
                //patchSpeedSrv = new PatchSpeedService(progressBar);
                DownloadPatch(PatchingService.GetPatchToPatch(0).GetName());
                playButton.Enabled = false;
                sidePanel.SetupNeedsPatchingUI();
            }
        }

        public bool IsReadyToPlay()
        {
            return playButton.Text.Equals("play");
        }

        private void GameModeLabel_MouseDown(object sender, MouseEventArgs e)
        {
            MovementService.SetMouseDownPoint(e);
        }

        private void GameModeLabel_MouseMove(object sender, MouseEventArgs e)
        {
            MovementService.MouseMove(this, e);
        }

        private void minimizeLabel_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("Sound_Click", sidePanel);
            WindowState = FormWindowState.Minimized;
        }

        private void exitLabel_MouseClick(object sender, MouseEventArgs e)
        {
            Application.Exit();
        }

        private void tcgLabel_MouseEnter(object sender, EventArgs e)
        {
            sidePanel.MouseEnter(tcgLabel);
        }

        private void exitLabel_MouseEnter(object sender, EventArgs e)
        {
            sidePanel.MouseEnter(exitLabel);
        }

        private void exitLabel_MouseLeave(object sender, EventArgs e)
        {
            sidePanel.MouseLeave(exitLabel);
        }

        private void tcgLabel_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", sidePanel);

            if (File.Exists(ConfigManager.GetGameDirectory() + @"\TradingCardGame\" + Program.TCG_APP))
                Process.Start(ConfigManager.GetGameDirectory() + @"\TradingCardGame\" + Program.TCG_APP);
            else
                MessageBox.Show("Error: The TCG application is missing.", "TCG Missing");
        }

        private void websiteLabel_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", sidePanel);
            Process.Start(Program.WEBSITE_URL);
        }

        private void discordLabel_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", sidePanel);
            Process.Start(Program.DISCORD_URL);
        }

        private void titleLabel_MouseDown(object sender, MouseEventArgs e)
        {
            MovementService.SetMouseDownPoint(e);
        }

        private string StringFormatFiles(List<string> files)
        {
            string filesString = string.Empty;

            for (int i = 0; i < files.Count; i++)
                filesString += "\n" + files[i];
            return filesString;
        }

        private void BackgroundPanel_MouseDown(object sender, MouseEventArgs e)
        {
            MovementService.SetMouseDownPoint(e);
        }

        private void BackgroundPanel_MouseMove(object sender, MouseEventArgs e)
        {
            MovementService.MouseMove(this, e);
        }

        private void LatestPatchLabel_Click(object sender, EventArgs e)
        {
            Process.Start("https://swghope.com/forums/showthread.php?" + PatchUrl);
        }

        private void LauncherMain_MouseEnter(object sender, EventArgs e)
        {
            if (ActiveArticle == null)
                return;
            ActiveArticle.BorderStyle = BorderStyle.None;
            ActiveArticle = null;
        }

        private void usernameLabel_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", sidePanel);
            if (AccountDropDown == null || AccountDropDown.IsDisposed)
            {
                AccountDropDown = new AccountDropDown(this);
                Controls.Add(AccountDropDown);
                return;
            }
            AccountDropDown.Dispose();
        }

        public Point GetAccountLabelLocation()
        {
            return usernameLabel.Location;
        }

        private void playButton_MouseEnter(object sender, EventArgs e)
        {
            sidePanel.MouseEnter(playButton);
        }

        private void playButton_MouseLeave(object sender, EventArgs e)
        {
            sidePanel.MouseLeave(playButton);
        }

        private void DonateLabel_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", sidePanel);
            Process.Start(Program.DONATE_URL);
        }
    }
}
