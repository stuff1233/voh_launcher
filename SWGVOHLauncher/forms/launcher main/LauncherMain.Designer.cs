﻿namespace SWGVOHLauncher
{
    partial class LauncherMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed
        /// otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LauncherMain));
            this.versionLabel = new System.Windows.Forms.Label();
            this.playButton = new System.Windows.Forms.Button();
            this.websiteLabel = new System.Windows.Forms.Label();
            this.discordLabel = new System.Windows.Forms.Label();
            this.exitLabel = new System.Windows.Forms.Label();
            this.tcgLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.PatchNotesTitleLabel = new System.Windows.Forms.Label();
            this.LatestPatchLabel = new System.Windows.Forms.Label();
            this.DonateLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.BackColor = System.Drawing.Color.Transparent;
            this.versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.versionLabel.Location = new System.Drawing.Point(1055, 575);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(33, 15);
            this.versionLabel.TabIndex = 6;
            this.versionLabel.Text = "v1.0";
            // 
            // playButton
            // 
            this.playButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.playButton.Enabled = false;
            this.playButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.playButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playButton.ForeColor = System.Drawing.SystemColors.Control;
            this.playButton.Location = new System.Drawing.Point(900, 525);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(150, 50);
            this.playButton.TabIndex = 5;
            this.playButton.Text = "play";
            this.playButton.UseVisualStyleBackColor = false;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            this.playButton.MouseEnter += new System.EventHandler(this.playButton_MouseEnter);
            this.playButton.MouseLeave += new System.EventHandler(this.playButton_MouseLeave);
            // 
            // websiteLabel
            // 
            this.websiteLabel.AutoSize = true;
            this.websiteLabel.BackColor = System.Drawing.Color.Transparent;
            this.websiteLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.websiteLabel.Enabled = false;
            this.websiteLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.websiteLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.websiteLabel.Location = new System.Drawing.Point(435, 5);
            this.websiteLabel.Name = "websiteLabel";
            this.websiteLabel.Size = new System.Drawing.Size(124, 37);
            this.websiteLabel.TabIndex = 23;
            this.websiteLabel.Text = "website";
            this.websiteLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.websiteLabel_MouseClick);
            // 
            // discordLabel
            // 
            this.discordLabel.AutoSize = true;
            this.discordLabel.BackColor = System.Drawing.Color.Transparent;
            this.discordLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.discordLabel.Enabled = false;
            this.discordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discordLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.discordLabel.Location = new System.Drawing.Point(552, 5);
            this.discordLabel.Name = "discordLabel";
            this.discordLabel.Size = new System.Drawing.Size(121, 37);
            this.discordLabel.TabIndex = 24;
            this.discordLabel.Text = "discord";
            this.discordLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.discordLabel_MouseClick);
            // 
            // exitLabel
            // 
            this.exitLabel.AutoSize = true;
            this.exitLabel.BackColor = System.Drawing.Color.Transparent;
            this.exitLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitLabel.ForeColor = System.Drawing.Color.LightGray;
            this.exitLabel.Location = new System.Drawing.Point(1050, 5);
            this.exitLabel.Name = "exitLabel";
            this.exitLabel.Size = new System.Drawing.Size(25, 29);
            this.exitLabel.TabIndex = 26;
            this.exitLabel.Text = "x";
            this.exitLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.exitLabel_MouseClick);
            this.exitLabel.MouseEnter += new System.EventHandler(this.exitLabel_MouseEnter);
            this.exitLabel.MouseLeave += new System.EventHandler(this.exitLabel_MouseLeave);
            // 
            // tcgLabel
            // 
            this.tcgLabel.AutoSize = true;
            this.tcgLabel.BackColor = System.Drawing.Color.Transparent;
            this.tcgLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tcgLabel.Enabled = false;
            this.tcgLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcgLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.tcgLabel.Location = new System.Drawing.Point(360, 5);
            this.tcgLabel.Name = "tcgLabel";
            this.tcgLabel.Size = new System.Drawing.Size(60, 37);
            this.tcgLabel.TabIndex = 28;
            this.tcgLabel.Text = "tcg";
            this.tcgLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tcgLabel_MouseClick);
            this.tcgLabel.MouseEnter += new System.EventHandler(this.tcgLabel_MouseEnter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(415, -1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 42);
            this.label1.TabIndex = 30;
            this.label1.Text = "|";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(530, -1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 42);
            this.label2.TabIndex = 31;
            this.label2.Text = "|";
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.BackColor = System.Drawing.Color.Transparent;
            this.usernameLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.usernameLabel.Location = new System.Drawing.Point(810, 5);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(188, 24);
            this.usernameLabel.TabIndex = 35;
            this.usernameLabel.Text = "Welcome Username!";
            this.usernameLabel.Visible = false;
            this.usernameLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.usernameLabel_MouseClick);
            // 
            // PatchNotesTitleLabel
            // 
            this.PatchNotesTitleLabel.AutoSize = true;
            this.PatchNotesTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.PatchNotesTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PatchNotesTitleLabel.ForeColor = System.Drawing.Color.White;
            this.PatchNotesTitleLabel.Location = new System.Drawing.Point(760, 539);
            this.PatchNotesTitleLabel.Name = "PatchNotesTitleLabel";
            this.PatchNotesTitleLabel.Size = new System.Drawing.Size(123, 16);
            this.PatchNotesTitleLabel.TabIndex = 41;
            this.PatchNotesTitleLabel.Text = "Latest Patch Notes:";
            // 
            // LatestPatchLabel
            // 
            this.LatestPatchLabel.AutoSize = true;
            this.LatestPatchLabel.BackColor = System.Drawing.Color.Transparent;
            this.LatestPatchLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LatestPatchLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LatestPatchLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.LatestPatchLabel.Location = new System.Drawing.Point(760, 557);
            this.LatestPatchLabel.Name = "LatestPatchLabel";
            this.LatestPatchLabel.Size = new System.Drawing.Size(123, 16);
            this.LatestPatchLabel.TabIndex = 42;
            this.LatestPatchLabel.Text = "Game Update 0.0.0";
            this.LatestPatchLabel.Click += new System.EventHandler(this.LatestPatchLabel_Click);
            // 
            // DonateLabel
            // 
            this.DonateLabel.AutoSize = true;
            this.DonateLabel.BackColor = System.Drawing.Color.Transparent;
            this.DonateLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DonateLabel.Enabled = false;
            this.DonateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DonateLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.DonateLabel.Location = new System.Drawing.Point(668, 5);
            this.DonateLabel.Name = "DonateLabel";
            this.DonateLabel.Size = new System.Drawing.Size(115, 37);
            this.DonateLabel.TabIndex = 43;
            this.DonateLabel.Text = "donate";
            this.DonateLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DonateLabel_MouseClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(646, -1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 42);
            this.label3.TabIndex = 44;
            this.label3.Text = "|";
            // 
            // LauncherMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1100, 620);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DonateLabel);
            this.Controls.Add(this.LatestPatchLabel);
            this.Controls.Add(this.PatchNotesTitleLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.websiteLabel);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tcgLabel);
            this.Controls.Add(this.exitLabel);
            this.Controls.Add(this.discordLabel);
            this.Controls.Add(this.playButton);
            this.Controls.Add(this.versionLabel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "LauncherMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SWG: VOH Launcher";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LauncherMain_FormClosing);
            this.Load += new System.EventHandler(this.LauncherMain_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LauncherMain_MouseDown);
            this.MouseEnter += new System.EventHandler(this.LauncherMain_MouseEnter);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LauncherMain_MouseMove);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.Button playButton;
        private System.Windows.Forms.Label websiteLabel;
        private System.Windows.Forms.Label discordLabel;
        private System.Windows.Forms.Label exitLabel;
        private System.Windows.Forms.Label tcgLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label PatchNotesTitleLabel;
        private System.Windows.Forms.Label LatestPatchLabel;
        private System.Windows.Forms.Label DonateLabel;
        private System.Windows.Forms.Label label3;
    }
}

