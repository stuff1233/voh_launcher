﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace SWGVOHLauncher.forms.alert
{
    public partial class Alert : UserControl
    {
        private readonly string Url;

        public Alert(string Text, string Url)
        {
            InitializeComponent();
            TitleLabel.Text = Text;
            this.Url = Url;
        }

        private void Alert_Load(object sender, EventArgs e)
        {
            Location = new Point(Parent.Width - Width, 45);
            ReadMoreLabel.Visible = !string.IsNullOrEmpty(Url);
        }

        private void ReadMoreLabel_MouseClick(object sender, MouseEventArgs e)
        {
            Process.Start(Url);
        }
    }
}
