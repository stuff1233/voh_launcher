﻿namespace SWGVOHLauncher.forms.alert
{
    partial class Alert
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TitleLabel = new System.Windows.Forms.Label();
            this.ReadMoreLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.ForeColor = System.Drawing.Color.White;
            this.TitleLabel.Location = new System.Drawing.Point(5, 9);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(75, 16);
            this.TitleLabel.TabIndex = 0;
            this.TitleLabel.Text = "Alert Title";
            // 
            // ReadMoreLabel
            // 
            this.ReadMoreLabel.AutoSize = true;
            this.ReadMoreLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ReadMoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReadMoreLabel.ForeColor = System.Drawing.Color.White;
            this.ReadMoreLabel.Location = new System.Drawing.Point(630, 9);
            this.ReadMoreLabel.Name = "ReadMoreLabel";
            this.ReadMoreLabel.Size = new System.Drawing.Size(88, 16);
            this.ReadMoreLabel.TabIndex = 1;
            this.ReadMoreLabel.Text = "Read More ➤";
            this.ReadMoreLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ReadMoreLabel_MouseClick);
            // 
            // Alert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.Controls.Add(this.ReadMoreLabel);
            this.Controls.Add(this.TitleLabel);
            this.DoubleBuffered = true;
            this.Name = "Alert";
            this.Size = new System.Drawing.Size(729, 34);
            this.Load += new System.EventHandler(this.Alert_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.Label ReadMoreLabel;
    }
}
