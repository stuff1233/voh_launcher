﻿namespace SWGVOHLauncher.forms.tools
{
    partial class Tools
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TreFixButton = new System.Windows.Forms.Button();
            this.ResourceManagerButton = new System.Windows.Forms.Button();
            this.ClosePanelButton = new System.Windows.Forms.Label();
            this.TitleLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TreFixButton
            // 
            this.TreFixButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreFixButton.Location = new System.Drawing.Point(120, 137);
            this.TreFixButton.Name = "TreFixButton";
            this.TreFixButton.Size = new System.Drawing.Size(110, 50);
            this.TreFixButton.TabIndex = 0;
            this.TreFixButton.Text = "tre fix";
            this.TreFixButton.UseVisualStyleBackColor = true;
            this.TreFixButton.Click += new System.EventHandler(this.TreFixButton_Click);
            // 
            // ResourceManagerButton
            // 
            this.ResourceManagerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResourceManagerButton.Location = new System.Drawing.Point(120, 193);
            this.ResourceManagerButton.Name = "ResourceManagerButton";
            this.ResourceManagerButton.Size = new System.Drawing.Size(110, 50);
            this.ResourceManagerButton.TabIndex = 1;
            this.ResourceManagerButton.Text = "resource manager";
            this.ResourceManagerButton.UseVisualStyleBackColor = true;
            this.ResourceManagerButton.Click += new System.EventHandler(this.ResourceManagerButton_Click);
            // 
            // ClosePanelButton
            // 
            this.ClosePanelButton.AutoSize = true;
            this.ClosePanelButton.BackColor = System.Drawing.Color.Transparent;
            this.ClosePanelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClosePanelButton.ForeColor = System.Drawing.Color.LightGray;
            this.ClosePanelButton.Location = new System.Drawing.Point(319, 0);
            this.ClosePanelButton.Name = "ClosePanelButton";
            this.ClosePanelButton.Size = new System.Drawing.Size(24, 25);
            this.ClosePanelButton.TabIndex = 53;
            this.ClosePanelButton.Text = "x";
            this.ClosePanelButton.Click += new System.EventHandler(this.ClosePanelButton_Click);
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.ForeColor = System.Drawing.Color.LightGray;
            this.TitleLabel.Location = new System.Drawing.Point(46, 13);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(325, 33);
            this.TitleLabel.TabIndex = 54;
            this.TitleLabel.Text = "swg vision of hope tools";
            // 
            // Tools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.TitleLabel);
            this.Controls.Add(this.ClosePanelButton);
            this.Controls.Add(this.ResourceManagerButton);
            this.Controls.Add(this.TreFixButton);
            this.Name = "Tools";
            this.Size = new System.Drawing.Size(346, 377);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button TreFixButton;
        private System.Windows.Forms.Button ResourceManagerButton;
        private System.Windows.Forms.Label ClosePanelButton;
        private System.Windows.Forms.Label TitleLabel;
    }
}
