﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SWGVOHLauncher.src.services.font;
using SWGVOHLauncher.forms.panel;
using SWGVOHLauncher.src.services.sound;
using System.IO;
using System.Diagnostics;
using SWGVOHLauncher.src.services.config;

namespace SWGVOHLauncher.forms.tools
{
    public partial class Tools : UserControl
    {
        private SidePanel sidePanel;

        public Tools(SidePanel sidePanel)
        {
            InitializeComponent();
            this.sidePanel = sidePanel;
            Location = new Point(425, 25);

            Control[] ControlsToUpdate = {
                ResourceManagerButton,
                TreFixButton,
                TitleLabel
            };

            foreach (Control control in ControlsToUpdate)
                control.Font = CustomFontService.CustomFontFont(control);

            TitleLabel.Location = new Point((Width - TitleLabel.Width) / 2, TitleLabel.Location.Y);
        }

        private void ClosePanelButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("Sound_Click", sidePanel);
            Dispose();
        }

        private void TreFixButton_Click(object sender, EventArgs e)
        {
            if (File.Exists(ConfigManager.GetGameDirectory() + @"\TREFix.exe"))
                Process.Start(ConfigManager.GetGameDirectory() + @"\TREFix.exe");
            SoundService.PlaySound("Sound_Click", sidePanel);
        }

        private void ResourceManagerButton_Click(object sender, EventArgs e)
        {
            if (File.Exists(ConfigManager.GetGameDirectory() + @"\ResourceManager.exe"))
                Process.Start(ConfigManager.GetGameDirectory() + @"\ResourceManager.exe");
            SoundService.PlaySound("Sound_Click", sidePanel);
        }
    }
}
