﻿/*************************************************************************
 * Copyright (C) 2016 <SWG Vision of Hope>
 *
 * This file is part of the SWG Vision of Hope Launcher.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *************************************************************************/
// @author: Iosnowore
using SWGVOHLauncher.src.services.font;
using SWGVOHLauncher.src.services.patching;
using System;
using System.Diagnostics;
using System.Security.Principal;
using System.Windows.Forms;

namespace SWGVOHLauncher
{
    static class Program
    {
        // URLs
        public const string CFG_URL = "https://geniusgaming.net/cfgs/";
        public const string DISCORD_URL = "https://discord.gg/unUFM93";
        public const string DONATE_URL = "https://swghope.com/forums/vbdonate.php?do=donate";
        public const string PATCH_URL = "https://geniusgaming.net/patches/";
        public const string WEBSITE_URL = "https://swghope.com/";
        
        
        

        // Game Connection Info
        public const string GAME_ADDRESS_LIVE = "login.swghope.com";
        public const string GAME_ADDRESS_TC   = "login1.swghope.com";

        // Application Names
        public const string TCG_APP          = "SWGTCG.exe";
        public const string LAUNCHER_PATCHER = "vohpatcher.exe";
        public const string NGE_CLIENT       = "SwgClient_r.exe";
        public const string NGE_SETUP        = "SwgClientSetup_r.exe";

        private static string Username;
        private static string Password;

        public static void SetUsername(string Username)
        {
            Program.Username = Username;
        }

        public static string GetUsername()
        {
            return Username;
        }

        public static void SetPassword(string Password)
        {
            Program.Password = Password;
        }

        public static string GetPassword()
        {
            return Password;
        }

        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (IsAlreadyRunning())
            {
                MessageBox.Show("An instance of the SWG Vision of Hope launcher already exists.", "Launcher already running");
                return;
            }

            if (PatchingService.LauncherNeedsUpdate())
            {
                MessageBox.Show("Launcher update v" + PatchingService.GetUpdatedLauncherVersion() + " is available!", "New launcher version available");
                Process.Start(Application.StartupPath + @"\" + LAUNCHER_PATCHER);
                Application.Exit();
                return;
            }

            Properties.Settings.Default.Upgrade();

            CustomFontService.LoadCustomFont();
            Application.Run(new LauncherMain());
        }

        private static bool IsAlreadyRunning()
        {
            int times = 0;
            foreach (Process clsProcess in Process.GetProcesses())
                if (clsProcess.ProcessName.Contains(Application.ProductName))
                    times++;
            return times > 1;
        }

        private static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
    }
}
