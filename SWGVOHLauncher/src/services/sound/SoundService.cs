﻿/*************************************************************************
 * Copyright (C) 2016 <SWG Vision of Hope>
 *
 * This file is part of the SWG Vision of Hope Launcher.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *************************************************************************/
// @author: Iosnowore
using SWGVOHLauncher.forms.panel;
using SWGVOHLauncher.src.services.config;
using System.IO;
using System.Media;

namespace SWGVOHLauncher.src.services.sound
{
    public class SoundService
    {
        public static void PlaySound(string sound, SidePanel panel)
        {
            if (ConfigManager.SFXIsEnabled())
                using (SoundPlayer player = new SoundPlayer((Stream)Properties.Resources.ResourceManager.GetObject(sound)))
                    player.Play();
        }
    }
}
