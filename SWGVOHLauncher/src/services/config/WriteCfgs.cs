﻿/*************************************************************************
 * Copyright (C) 2017 <SWG Vision of Hope>
 *
 * This file is part of the SWG Vision of Hope Launcher.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *************************************************************************/
// @author: Iosnowore
using SWGVOHLauncher.forms.panel;
using System.IO;

namespace SWGVOHLauncher.src.services.config
{
    public class WriteCfgs
    {
        private static bool EnableStaffCfg;

        public static void WriteLogin(SidePanel panel)
        {
            using (StreamWriter newConfig = new StreamWriter(ConfigManager.GetGameDirectory() + @"\login.cfg"))
            {
                newConfig.WriteLine("[ClientGame]");
                newConfig.WriteLine("\tdisableCutScenes=" + (ConfigManager.DisableCutScenes() ? 1 : 0));
                newConfig.WriteLine("\tloginClientID=\"" + Program.GetUsername() + "\"");
                newConfig.WriteLine("\tloginClientPassword=\"" + Program.GetPassword() + "\"");
                newConfig.WriteLine("\tskipSplash=" + (ConfigManager.SkipSplash() ? 1 : 0));
                if (EnableStaffCfg)
                    newConfig.WriteLine("\t0fd435d9=true");
                newConfig.Close();
            }
        }

        public static void EnableStaff()
        {
            EnableStaffCfg = true;
        }
    }
}
