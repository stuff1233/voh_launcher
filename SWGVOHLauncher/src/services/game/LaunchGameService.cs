﻿/*************************************************************************
 * Copyright (C) 2017 <SWG Vision of Hope>
 *
 * This file is part of the SWG Vision of Hope Launcher.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *************************************************************************/
// @author: Iosnowore
using IWshRuntimeLibrary;
using SWGVOHLauncher.src.services.config;
using SWGVOHLauncher.src.services.sound;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace SWGVOHLauncher.src.services.game
{
    class LaunchGameService
    {
        public static void LaunchGame(String gameDir, LauncherMain main)
        {
            if (!Directory.Exists(gameDir))
            {
                MessageBox.Show("Error: Please configure your SWG Vision of Hope game directory.", "Invalid Game Directory!");
                return;
            }

            SoundService.PlaySound("Sound_Play", main.GetSidePanel());

            WriteCfgs.WriteLogin(main.GetSidePanel());

            CreateShortcut("Client");

            using (Process p = new Process())
            {
                p.StartInfo = new ProcessStartInfo(Application.StartupPath + @"\SWG Vision of Hope Client.lnk");
                if (ConfigManager.RunClientAsAdmin())
                    p.StartInfo.Verb = "runas";
                p.Start();
            }
        }

        public static void CreateShortcut(string shortcutType)
        {
            string shortcutLocation = Path.Combine(Application.StartupPath, "SWG Vision of Hope " + shortcutType + ".lnk");
            IWshShortcut shortcut = (IWshShortcut)new WshShell().CreateShortcut(shortcutLocation);
            shortcut.Description = "SWG Vision of Hope " + shortcutType;

            shortcut.TargetPath = ConfigManager.GetGameDirectory() + @"\" + (shortcutType.Equals("Client") ? Program.NGE_CLIENT : Program.NGE_SETUP);
            shortcut.WorkingDirectory = ConfigManager.GetGameDirectory();
            shortcut.Save();
        }
    }
}
