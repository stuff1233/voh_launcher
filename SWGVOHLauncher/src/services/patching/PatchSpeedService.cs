﻿/*************************************************************************
 * Copyright (C) 2017 <SWG Vision of Hope>
 *
 * This file is part of the SWG Vision of Hope Launcher.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *************************************************************************/
// @author: Iosnowore
using SWGVOHLauncher.src.custom;
using System;
using System.Windows.Forms;

namespace SWGVOHLauncher.src.services.patching
{
    public class PatchSpeedService
    {
        private CustomProgressBar progressBar;
        private readonly Timer timer = new Timer();
        private string lastPatch;
        private string patch;
        private double speed;
        private long BytesIncoming = 0;

        public void AddIncomingBytes(long Bytes)
        {
            BytesIncoming += Bytes / 10;
        }

        public PatchSpeedService(CustomProgressBar progressBar)
        {
            this.progressBar = progressBar;
            timer.Tick += Timer_Tick;
            timer.Interval = 1000;
            timer.Enabled = true;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            //speed = Math.Round(((double)(BytesIncoming * 10) / 1000000), 2);
            //BytesIncoming = 0;
            //progressBar.SetText("Installing..\t" + Math.Round((((double)progressBar.Value / progressBar.Maximum) * 100), 2) + "% (" + speed + " MB/s)");
            progressBar.SetText("Installing..\t" + Math.Round((((double)progressBar.Value / progressBar.Maximum) * 100), 2) + "%");
        }

        public void UpdatePatch(string patch)
        {
            this.patch = patch;
            //progressBar.SetText("Installing..\t" + Math.Round((((double)progressBar.Value / progressBar.Maximum) * 100), 2) + "% (" + speed + " MB/s)");
            progressBar.SetText("Installing..\t" + Math.Round((((double)progressBar.Value / progressBar.Maximum) * 100), 2) + "%");
        }

        public void StopTimer()
        {
            timer.Enabled = false;
        }
    }
}
