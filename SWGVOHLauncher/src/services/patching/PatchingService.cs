﻿/*************************************************************************
 * Copyright (C) 2017 <SWG Vision of Hope>
 *
 * This file is part of the SWG Vision of Hope Launcher.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *************************************************************************/
// @author: Iosnowore
using SWGVOHLauncher.src.services.config;
using SWGVOHLauncher.src.services.Reader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace SWGVOHLauncher.src.services.patching
{
    public class PatchingService
    {
        private static readonly List<Patch> VALID_PATCHES = new List<Patch>();
        private static readonly List<Patch> PATCHES_TO_PATCH = new List<Patch>();
        private static string newestLauncherVersion;
        private static bool patchServerIsOffline;

        private const string PatchCfg = "patches.txt";

        private static readonly string[] TCG_PATCHES = {
            "Cards.dll",
            "Custom.dll",
            "Effects.dll",
            "login_tcg.cfg",
            "NAudio.dll",
            "NAudio.Vorbis.dll",
            "NVorbis.dll",
            "Resources.dll",
            "Sounds.dll",
            "String.dll",
            "SWGTCG.dll",
            "SWGTCG.exe",
            "TCGData.dll"
        };

        private static readonly string[] FILES_TO_IGNORE = {
            "UserConfigEditor.exe",
            "user.cfg",
            "ui.log",
            "options.cfg",
            "login.cfg",
            "local_machine_options.iff"
        };

        private static readonly string[] FOLDERS_TO_INGORE = {
            "miles",
            "profiles",
            "screenshots",
            "string",
            "TradingCardGame"
        };

        private const string ILM_ANIMATION = "ILM_animation.tre";
        private const string ILM_MAPS = "ILM_maps.tre";
        private const string ILM_MUSIC = "ILM_music.tre";
        private const string ILM_SOUND = "ILM_sound.tre";
        private const string ILM_UI = "ILM_UI.tre";
        private const string ILM_VISUALS_1 = "ILM_visuals_1.tre";
        private const string ILM_VISUALS_2 = "ILM_visuals_2.tre";
        private static readonly string[] ILM_PATCHES = {
            ILM_ANIMATION,
            ILM_MAPS,
            ILM_MUSIC,
            ILM_SOUND,
            ILM_UI,
            ILM_VISUALS_1,
            ILM_VISUALS_2
        };
        private static readonly long[] ILM_PATCH_SIZES = {
            1673120,
            109034505,
            104810794,
            182506492,
            692996,
            869178285,
            882142462
        };

        private static void ReadAndSetPatches()
        {
            string Patches = WebClientReader.GetWebPageContent(Program.CFG_URL + PatchCfg);
            using (StringReader Reader = new StringReader(Patches))
            {
                string[] Line = Reader.ReadLine().Split('\t');
                while (Line != null)
                {
                    VALID_PATCHES.Add(new Patch(Line[0], long.Parse(Line[1])));
                    string NextLine = Reader.ReadLine();
                    Line = NextLine?.Split('\t');
                }
            }
            for (int i = 0; i < ILM_PATCHES.Length; i++)
            {
                if (CanPatchILMFile(ILM_PATCHES[i]))
                    VALID_PATCHES.Add(new Patch(ILM_PATCHES[i], ILM_PATCH_SIZES[i]));
            }
        }

        public static void SetPatches(string gameDir)
        {
            VALID_PATCHES.Clear();
            PATCHES_TO_PATCH.Clear();
            ReadAndSetPatches();

            foreach (Patch patch in VALID_PATCHES)
            {
                if (!PATCHES_TO_PATCH.Contains(patch))
                {
                    string localPatchDir = GetPatchLocalDir(patch.GetName(), gameDir);

                    if (!File.Exists(localPatchDir))
                        PATCHES_TO_PATCH.Add(patch);
                    else
                    {
                        long sizeDiff = new FileInfo(localPatchDir).Length - patch.GetBytes();

                        if (!patch.GetName().Equals("live.cfg") && sizeDiff > 0)
                            PATCHES_TO_PATCH.Add(patch);
                        else if (sizeDiff > 7 || sizeDiff < -7)
                            PATCHES_TO_PATCH.Add(patch);
                    }
                }
            }
        }

        private static bool CanPatchILMFile(string File)
        {
            switch (File)
            {
                case ILM_ANIMATION:
                    return ConfigManager.ConfigValueIsTrue("enableILMAnimations");
                case ILM_MAPS:
                    return ConfigManager.ConfigValueIsTrue("enableILMPlanetMaps");
                case ILM_MUSIC:
                    return ConfigManager.ConfigValueIsTrue("enableILMMusic");
                case ILM_SOUND:
                    return ConfigManager.ConfigValueIsTrue("enableILMSoundEffects");
                case ILM_UI:
                    return ConfigManager.ConfigValueIsTrue("enableILMUserInterface");
                case ILM_VISUALS_1:
                case ILM_VISUALS_2:
                    return ConfigManager.ConfigValueIsTrue("enableILMGraphics");
                default:
                    return false;
            }
        }

        public static int GetPatchesToPatchCount()
        {
            return PATCHES_TO_PATCH.Count;
        }

        public static int GetTotalPatchSizes()
        {
            long total = 0;

            foreach (Patch patch in PATCHES_TO_PATCH)
                total += patch.GetBytes();
            return (int)(total / 1000);
        }

        public static Patch GetPatchToPatch(int index)
        {
            return PATCHES_TO_PATCH[index];
        }

        public static void RemovePatchToPatch(int index)
        {
            PATCHES_TO_PATCH.Remove(PATCHES_TO_PATCH[index]);
        }

        public static List<Patch> GetPatchNames()
        {
            return VALID_PATCHES;
        }

        public static String GetUpdatedLauncherVersion()
        {
            return newestLauncherVersion;
        }

        public static bool LauncherNeedsUpdate()
        {

            newestLauncherVersion = WebClientReader.GetWebPageContent(Program.CFG_URL + "launcher.txt");
            return !newestLauncherVersion.Equals(Application.ProductVersion);
        }

        public static bool GetPatchServerIsOffline()
        {
            return patchServerIsOffline;
        }

        public static void UpdatePatcher()
        {
            using (WebClient client = new WebClient())
                client.DownloadFile(new Uri(Program.LAUNCHER_PATCHER), Application.StartupPath + @"\" + Program.LAUNCHER_PATCHER);
        }

        public static string GetPatchDownloadURL(string patch)
        {
            string url = Program.PATCH_URL;

            string fileType = patch.Substring(patch.Length - 3);

            if (fileType.Equals("flt") || fileType.Equals("m3d") || fileType.Equals("asi"))
                url += "miles/";
            else if (fileType.Equals("stf"))
                url += "string/en/";
            else if (TCG_PATCHES.Contains(patch))
                url += "TradingCardGame/";

            return url + patch;
        }

        public static string GetPatchLocalDir(string patch, string gameDir)
        {
            if (patch.Equals(Program.LAUNCHER_PATCHER))
                return Application.StartupPath + @"\" + patch;

            string dir = gameDir + @"\";
            string fileType = patch.Substring(patch.Length - 3);

            if (fileType.Equals("flt") || fileType.Equals("m3d") || fileType.Equals("asi"))
                dir += @"miles\";
            else if (fileType.Equals("stf"))
                dir += @"string\en\";
            else if (TCG_PATCHES.Contains(patch))
                dir += @"TradingCardGame\";

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            return dir + patch;
        }

        public static bool FileIsValidGameFile(string patchName)
        {
            foreach (Patch patch in VALID_PATCHES)
                if (patchName.Equals(patch.GetName()) || FILES_TO_IGNORE.Contains(patchName) || patchName.StartsWith("characterlist_") || patchName.Equals(Program.LAUNCHER_PATCHER) || patchName.Equals("SWG Vision of Hope Launcher.exe") || patchName.Substring(patchName.Length - 4).Equals(".lnk") || patchName.Equals("Backgrounds.dll"))
                    return true;
            return false;
        }

        public static bool FolderIsValidGameFolder(string folderName)
        {
            foreach (string folder in FOLDERS_TO_INGORE)
                if (folderName.Equals(folder))
                    return true;
            return false;
        }
    }
}
