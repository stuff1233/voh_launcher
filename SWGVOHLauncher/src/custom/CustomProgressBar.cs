﻿/*************************************************************************
 * Copyright (C) 2017 <SWG Vision of Hope>
 *
 * This file is part of the SWG Vision of Hope Launcher.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *************************************************************************/
// @author: Iosnowore
using SWGVOHLauncher.src.services.font;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SWGVOHLauncher.src.custom
{
    public enum ProgressBarDisplayText
    {
        Percentage,
        CustomText
    }

    public class CustomProgressBar : ProgressBar
    {
        //Property to set to decide whether to print a % or Text
        public ProgressBarDisplayText DisplayStyle { get; set; }

        public static readonly string[] LAUNCHER_STATES = {
            "ready to play",
            "needs patching",
            "awaiting login",
            "invalid game directory"
        };

        public string GetText()
        {
            return Text;
        }

        public void SetText(string text)
        {
            Text = text;
        }

        public CustomProgressBar()
        {
            // Modify the ControlStyles flags
            //http://msdn.microsoft.com/en-us/library/system.windows.forms.controlstyles.aspx

            SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle rect = ClientRectangle;
            Graphics g = e.Graphics;

            ProgressBarRenderer.DrawHorizontalBar(g, rect);
            //rect.Inflate(-3, -3); // This is a border
            if (Value > 0)
            {
                // As we doing this ourselves we need to draw the chunks on the progress bar
                Rectangle clip = new Rectangle(rect.X, rect.Y, (int)Math.Round(((float)Value / Maximum) * rect.Width), rect.Height);
                ProgressBarRenderer.DrawHorizontalChunks(g, clip);
            }

            // Set the Display text (Either a % amount or our custom text)

            using (Font f = CustomFontService.CustomFontFont(16))
            {
                SizeF len = g.MeasureString(Text, f);
                // Calculate the location of the text (the middle of progress bar)
                // The commented-out code will centre the text into the highlighted area only. This will centre the text regardless of the highlighted area.
                // Draw the custom text
                g.DrawString(Text, f, Brushes.Black, new Point(20, Convert.ToInt32((Height - len.Height) / 2) + 2));
            }
        }
    }
}
