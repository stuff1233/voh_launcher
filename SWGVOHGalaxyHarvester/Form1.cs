﻿using System;
using System.Net;
using System.Windows.Forms;

namespace SWGVOHGalaxyHarvester
{
    public partial class Form1 : Form
    {
        private new MainForm Parent;

        public Form1(MainForm Parent)
        {
            InitializeComponent();
            this.Parent = Parent;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string Data = new WebClient().DownloadString("https://geniusgaming.net/status/res/resources.txt");
            string[] ResourcesString = Data.Split(new string[] { "},{" }, StringSplitOptions.None);
            Program.SetResourcesLength(ResourcesString.Length);
            string FinalText = string.Empty;

            for (int i = 0; i < ResourcesString.Length; i++)
            {
                Program.SetResource(ResourcesString[i], i);
                FinalText += Program.GetResource(i).ToString();
            }
            Parent.SetTextBoxText(FinalText);
            Parent.AddRangeToClassListBox();
            Dispose();
        }
    }
}
