﻿namespace SWGVOHLauncherPatcher
{
    partial class PatcherMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PatcherMain));
            this.WaitPatchingLabel = new System.Windows.Forms.Label();
            this.PatchingProgressBar = new System.Windows.Forms.ProgressBar();
            this.PercentFinished = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // WaitPatchingLabel
            // 
            this.WaitPatchingLabel.AutoSize = true;
            this.WaitPatchingLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.WaitPatchingLabel.Location = new System.Drawing.Point(1, 70);
            this.WaitPatchingLabel.Name = "WaitPatchingLabel";
            this.WaitPatchingLabel.Size = new System.Drawing.Size(255, 26);
            this.WaitPatchingLabel.TabIndex = 0;
            this.WaitPatchingLabel.Text = "Please wait while the SWG Vision of Hope Launcher\r\nis updating...";
            this.WaitPatchingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PatchingProgressBar
            // 
            this.PatchingProgressBar.Location = new System.Drawing.Point(11, 141);
            this.PatchingProgressBar.Name = "PatchingProgressBar";
            this.PatchingProgressBar.Size = new System.Drawing.Size(234, 23);
            this.PatchingProgressBar.TabIndex = 1;
            // 
            // PercentFinished
            // 
            this.PercentFinished.AutoSize = true;
            this.PercentFinished.ForeColor = System.Drawing.SystemColors.Control;
            this.PercentFinished.Location = new System.Drawing.Point(97, 176);
            this.PercentFinished.Name = "PercentFinished";
            this.PercentFinished.Size = new System.Drawing.Size(63, 13);
            this.PercentFinished.TabIndex = 2;
            this.PercentFinished.Text = "0% Finished";
            // 
            // PatcherMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.ClientSize = new System.Drawing.Size(257, 261);
            this.Controls.Add(this.PercentFinished);
            this.Controls.Add(this.PatchingProgressBar);
            this.Controls.Add(this.WaitPatchingLabel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PatcherMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SWG: VOH Patcher";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PatcherMain_FormClosing);
            this.Load += new System.EventHandler(this.PatcherMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label WaitPatchingLabel;
        private System.Windows.Forms.ProgressBar PatchingProgressBar;
        private System.Windows.Forms.Label PercentFinished;
    }
}

