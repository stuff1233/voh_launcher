﻿namespace SWGVOHTREFix
{
    partial class TREFix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TREFix));
            this.OutputListbox = new System.Windows.Forms.ListBox();
            this.ScanButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // OutputListbox
            // 
            this.OutputListbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.OutputListbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputListbox.FormattingEnabled = true;
            this.OutputListbox.ItemHeight = 16;
            this.OutputListbox.Location = new System.Drawing.Point(4, 6);
            this.OutputListbox.Name = "OutputListbox";
            this.OutputListbox.Size = new System.Drawing.Size(300, 130);
            this.OutputListbox.TabIndex = 0;
            // 
            // ScanButton
            // 
            this.ScanButton.Location = new System.Drawing.Point(117, 142);
            this.ScanButton.Name = "ScanButton";
            this.ScanButton.Size = new System.Drawing.Size(75, 23);
            this.ScanButton.TabIndex = 1;
            this.ScanButton.Text = "Scan";
            this.ScanButton.UseVisualStyleBackColor = true;
            this.ScanButton.Click += new System.EventHandler(this.ScanButton_Click);
            // 
            // TREFix
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.ClientSize = new System.Drawing.Size(309, 172);
            this.Controls.Add(this.ScanButton);
            this.Controls.Add(this.OutputListbox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TREFix";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TRE Fix";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox OutputListbox;
        private System.Windows.Forms.Button ScanButton;
    }
}

