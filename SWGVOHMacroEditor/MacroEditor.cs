﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace SWGVOHMacroEditor
{
    public partial class MacroEditor : Form
    {
        private static readonly string profilesDirectory = Application.StartupPath + @"\profiles";
        private static readonly List<Macro> Macros = new List<Macro>();

        public MacroEditor()
        {
            InitializeComponent();

            if (Directory.Exists(profilesDirectory))
            {
                string[] folders = Directory.GetDirectories(profilesDirectory);
                foreach (string x in folders)
                    ProfileUserComboBox.Items.Add(x.Replace(profilesDirectory + @"\", string.Empty));
            }
        }

        private void ProfileUserComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //NewMacroButton.Enabled = true;
            MacroNameComboBox.Enabled = true;
            MacroNameComboBox.Items.Clear();
            Macros.Clear();

            if (!File.Exists(profilesDirectory + @"\" + ProfileUserComboBox.Text + @"\macros.txt"))
                return;

            using (StreamReader reader = new StreamReader(profilesDirectory + @"\" + ProfileUserComboBox.Text + @"\macros.txt"))
            {
                string[] fileLines = reader.ReadToEnd().Split('\n');
                for (int i = 0; i < fileLines.Length; i++)
                    if (i > 0 && !string.IsNullOrWhiteSpace(fileLines[i]))
                    {
                        Macros.Add(new Macro(fileLines[i]));
                        MacroNameComboBox.Items.Add(Macros[i - 1].GetName());
                    }
            }
            MacroNameComboBox.SelectedIndex = 0;
        }

        private void MacroNameComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            foreach (Macro macro in Macros)
                if (macro.GetName().Equals(MacroNameComboBox.Text))
                    MacroTextBox.Text = macro.GetContents().Replace("#ffffff ", string.Empty).Replace(";", ";\n");
        }

        private void MacroTextBox_MouseDown(object sender, MouseEventArgs e)
        {
            // TODO: Detect change in macro, determines if save button is enabled/disabled
        }
    }
}
