﻿using System;
using System.IO;
using System.Windows.Forms;

namespace UserCfgManager
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private const string USER_CFG = "user.cfg";

        private void MainForm_Load(object sender, EventArgs e)
        {
            // TODO: Load values from existing user.cfg
            ZoomTextBox.Text = "1";
        }

        private void CreateButton_Click(object sender, EventArgs e)
        {
            // TODO: Export user.cfg
            using (StreamWriter writer = new StreamWriter(Application.StartupPath + @"\" + USER_CFG))
            {
                writer.WriteLine("[ClientGame]");
                writer.WriteLine("\t0fd345d9=" + GodModeCommandsCheckBox.Checked.ToString().ToLower());
                writer.WriteLine("\tfreeChaseCameraMaximumZoom=" + ZoomTextBox.Text);
                writer.WriteLine();
                writer.WriteLine("[ClientUserInterface]");
                writer.WriteLine("\tdebugExamine=" + (DebugExamineCheckBox.Checked ? 1 : 0));
                writer.WriteLine("\tdebugClipboardExamine=" + (DebugClipboardExamineCheckBox.Checked ? 1 : 0));
                writer.WriteLine("\tallowTargetAnything=" + (AllowTargetAnythingCheckBox.Checked ? 1 : 0));
                writer.WriteLine("\tdrawNetworkIds=" + (DrawNetworkIdsCheckBox.Checked ? 1 : 0));
                writer.Close();
            }
        }

        private void ExtendMaxZoomCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            ZoomTextBox.Text = "1";
            ZoomTextBox.Visible = !ZoomTextBox.Visible;
        }
    }
}
