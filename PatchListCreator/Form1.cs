﻿using System;
using System.IO;
using System.Windows.Forms;

namespace PatchListCreator
{
    public partial class PatchListCreator : Form
    {
        public PatchListCreator()
        {
            InitializeComponent();
        }

        private void PatchDirectory_MouseClick(object sender, MouseEventArgs e)
        {
            using (FolderBrowserDialog Dialog = new FolderBrowserDialog())
            {
                Dialog.Description = "Please select the patch directory";

                if (Dialog.ShowDialog() == DialogResult.OK)
                {
                    PatchDirectory.Text = Dialog.SelectedPath;
                }
            }
        }

        private void CreateButton_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(PatchDirectory.Text))
                return;

            string[] Files = Directory.GetFiles(PatchDirectory.Text, "*", SearchOption.AllDirectories);

            using (StreamWriter Writer = new StreamWriter(Application.StartupPath + @"\patches.txt"))
            {
                foreach (string File in Files)
                {
                    Writer.WriteLine(new FileInfo(File).Name + "\t" + new FileInfo(File).Length);
                }
                Writer.Close();
            }
        }
    }
}
